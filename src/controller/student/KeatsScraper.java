package controller.student;

import java.util.Map;

import javax.swing.JOptionPane;

import model.student.Student;
import model.student.keatsEntry;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Takes login information and destination page to scrape HTML data from KEATS. 
 * Makes use of the JSoup library. Licensed under the liberal MIT license
 * {@link http://jsoup.org}
 * @author Alex Gubbay
 *
 */
public class KeatsScraper {

	private Map<String, String> sessionCookies;
	private String username;
	private String password;
	private String URL;
	private Document participationPage;
	private String module;
	public KeatsScraper() {

	}
/**
 * Used to scrape the HTML data if no login is required to access the data.
 * Does not store cookies for session data.
 * @param URL the URL of the page on which the data to be retrieved is.
 * @param module the module code that the data should be assigned to.
 */
	public void scrapeWithoutCredentials(String URL, String module) {
		this.URL=URL;
		this.module=module;
		System.setProperty("jsse.enableSNIExtension", "false");
		try {
			scrape();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
/**
 *  Scrapes data from page provided in URL after logging into keats to gain access
 *  using log in credentials provided.
 * @param URL the URL of the page with the data to be scraped
 * @param username the KEATS login username of the user.
 * @param password the KEATS password of the user.
 * @param module the module the participation data is to be assigned to.
 * @throws Exception Exceptions that could be thrown by the process.
 */
	public void scrapeWithCredentials(String URL, String username,
			String password, String module) throws Exception {
		
		this.URL = URL;
		this.username = username;
		this.password = password;
		this.module=module;
		/*
		 * set java to ignore a misconfigured SSL handshake from KEATS which
		 * otherwise would cause an ' SSL handshake alert' error.
		 */
		System.setProperty("jsse.enableSNIExtension", "false");

			/*
			 * opens a connection to the KEATS login page, in order to obtain a
			 * Session cookie to allow access to files behind the login.
			 */
			Connection.Response loginForm = Jsoup
					.connect("https://keats.kcl.ac.uk/login/index.php")
					.method(Connection.Method.GET).execute();
			/*
			 * This will POST the login data to the KEATS page, return the
			 * "my home" page and creating session cookies. This will allow us
			 * to access the page behind the login.
			 */
			Connection.Response home = Jsoup
					.connect("https://keats.kcl.ac.uk/login/index.php")
					.data("username", this.username)
					// username and
					.data("password", this.password)
					// and password go here
					.data("loginbtn", "Log in").cookies(loginForm.cookies())
					.method(Method.POST).execute();

			/* cookies created are stored here. They are stored as a member variable
			 * so as to allow the scrape() method to maintain session. 
			 */
			sessionCookies = home.cookies();

			/*
			 * Scrape method code is essentially the same regardless of it needs to 
			 * use cookies to maintain session or not.
			 */
			scrape();

	}

	/*
	 * This is the same regardless of if it is behind a login or not.
	 * The method will check of session cookies exist and if they do, use
	 * them to get access to the data table page. 
	 * It also deannoymises the data row by row as it is downloaded.
	 */
	private void scrape() throws Exception {
		
	
			if (!sessionCookies.isEmpty()) {
				/*
				 * connects using session cookies to maintain session from login.
				 */
				participationPage = Jsoup.connect(this.URL)
						.cookies(sessionCookies).get();
			} else {
				/*
				 * connects without session cookies. This will fail is login credentials
				 * are required.
				 */
				participationPage = Jsoup.connect(this.URL).get();
			}

			// gets all elements with that are tagged as table rows.
			Elements e = participationPage.select("tr");
			for (int i = 1; i < e.size() - 1; i++) {

				// gets each row of the table individually.
				Element temp = e.get(i);
				// gets the data out of each cell in the row.

				Elements data = temp.select("td");
				// if any of the interesting cells have data
				if ((data.get(2).hasText()) || (data.get(3).hasText())
						|| data.get(6).hasText()) {

					// create a new keatsEntry and add it to the ArrayList
					keatsEntry rowData = new keatsEntry(data.get(2).text(),
							data.get(3).text(), data.get(6).text(),module);
					
					String tempEmail=rowData.getEmail();
					String tempName=rowData.getName();
					//find student by email
					Student tempStudent = StudentDataManager.getStudentByEmail(tempEmail);
					//add the data to the student
					if(tempStudent!=null){
						tempStudent.addParticipationData(rowData);
					//if finding by email failed try the email name.
					}else if(tempStudent==null){
						tempStudent = StudentDataManager.getStudent(tempName);
						if(tempStudent!=null){
							tempStudent.addParticipationData(rowData);
						//student wasn't found
						}else if(tempStudent==null){
							JOptionPane.showMessageDialog(null, "Unable to identify student with name: "+tempName);
						}
					}

				} else {
					break;
					/*
					 * There are 100s of empty rows at the bottom of the table.
					 * this stops them being checked.
					 */
				}
			}
		

		
		

	}

}
