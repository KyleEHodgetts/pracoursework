package controller.student;

import java.util.ArrayList;

import javax.swing.JProgressBar;

import model.result.Assessment;
import model.student.Student;
import studentdata.DataTable;

/**
 * 
 * @author Kyle Hodgetts
 * @author Alex Gubbay
 * @author Peter Barta
 * @version 1.0
 * 
 * <p>Data manager for students that pulls information from the server</p>
 *
 */
/**
 *
 *
 */
public class StudentDataManager {

	private static ArrayList<Student> arryListStudentRecords=new ArrayList<Student>();
	private static ArrayList<Assessment> arryListStudentAssessments=new ArrayList<Assessment>();
	private	static DataTable studentsFromServer;
	private static ArrayList<Assessment> arryListNewStudentAssessments=new ArrayList<Assessment>();
	
	/**
	 * Creates the record manager, which downloads the information from the server.
	
	 *
	 */
	
	
	public static void initateRecordsManager(JProgressBar dataImportbar){
		// creates a new ServerConnector object.
		ServerConnector serverConnection1 = new ServerConnector();
		dataImportbar.setValue(1);
		dataImportbar.setStringPainted(true);
		dataImportbar.setString("Opening Connection To Server...");
		dataImportbar.repaint();
		// creates a new DataTable of Student Objects
		studentsFromServer = serverConnection1.getStudentRecord();
		dataImportbar.setValue(2);
		dataImportbar.repaint();
		
		int rowCount = studentsFromServer.getRowCount();
		dataImportbar.setString("Importing Data 0 of "+rowCount);
		dataImportbar.setValue(3);
		dataImportbar.repaint();
		for (int row = 0; row < rowCount; ++row) {
			Student s = new Student(studentsFromServer.getCell(row, 2),
					Integer.parseInt(studentsFromServer.getCell(row, 3)),
					studentsFromServer.getCell(row, 1),
					studentsFromServer.getCell(row, 0));
		
			arryListStudentRecords.add(s);
			dataImportbar.setValue(row+4);
			dataImportbar.setString("Importing Data "+row+" of "+rowCount);
			dataImportbar.repaint();
		}
		dataImportbar.setValue(216);
		dataImportbar.repaint();
	}
	
	
	/**
	 * @return number of students in the record
	 */
	public static int getRecordSize(){
		return arryListStudentRecords.size();
	}
	
	
	/**
	 * @param StudentNumber StudentNumber of student to receive the annon marking code.
	 * @param annonMarkingCode MarkingCode to amend the student record with.
	 * @return returns the success or failure of the attempt to find and modify the record.
	 */
	public static boolean addAnnonCode(int StudentNumber, String annonMarkingCode){

		for(int i=0; i<arryListStudentRecords.size(); i++){
			if(arryListStudentRecords.get(i).getID()==StudentNumber){
				arryListStudentRecords.get(i).addAnnonMarkingCodeStudent(annonMarkingCode);
				return false;
				
			}
		}
		
		return true;
	}
	
	
	/**
	 * @param studentName Name of student whos record is requested.
	 * @return returns the student whos name matches the param passed into the method.
	 */
	public static Student getStudent(String studentName){
		Student s = null;
		for(int i=0; i<arryListStudentRecords.size(); i++){
		
			if(arryListStudentRecords.get(i).getName().contains(studentName)){
				s=arryListStudentRecords.get(i);
				break;
			}
		}
		return s;
	}
	
	
	/**
	 * @return returns an ArrayList of all of the students in the record.
	 */
	public static ArrayList<Student> getStudents(){
		return arryListStudentRecords;
	}
	
	
	/**
	 * @param annonMarkingCode annonMarkingCode of student to be located.
	 * @return Student whos annonMarkingCode matches the param passed into the method.
	 */
	public static Student getStudentByannonCode(String annonMarkingCode){
//		String seperatedCode=annonMarkingCode.substring(0, annonMarkingCode.length()-2);
		for (int i = 0; i < arryListStudentRecords.size(); i++) {
			if(arryListStudentRecords.get(i).getAnnonMarkingCode().equals(annonMarkingCode)){
				return arryListStudentRecords.get(i);
			}
		}
		return null;
	}

	public static Student getStudentByEmail(String email){
		for (int i = 0; i < arryListStudentRecords.size(); i++) {
			if(arryListStudentRecords.get(i).getEmail().matches(email)){
				return arryListStudentRecords.get(i);
			}
		}
		return null;
	}

	
	/**
	 * @param a ArrayList of assessments to be added to the records.
	 */
	public static void importAssessments(ArrayList<Assessment> a){
			//adds the new assessments to the ArrayList of assessments
			arryListStudentAssessments.addAll(a);
			//sets the arrayList of latest imports to the arraylist in the method call.
			arryListNewStudentAssessments=a;
		} 
		
		
	/**
	 * Gets ArrayList of all assessments previously added.
	 * @return returns the assessments in the records currently
	 */
	public static ArrayList<Assessment> getAssessments(){
		return arryListStudentAssessments;
	}
	
	
	/**
	 * Gets the number of assessments that have been loaded.
	 * @return the number of assessments that have been imported.
	 */
	public  static int getNumberOfAssessments(){
		return arryListStudentAssessments.size();
	}
	/**
	 * Removes all of the annon marking codes from the students in the data
	 * base to make sure only one set of marking codes is used at any one time.
	 * This does not affect the results already added.
	 */
	public static void clearCodes(){
		for (int i = 0; i < arryListStudentRecords.size(); i++) {
			arryListStudentRecords.get(i).removeMarkingCode();
			}
		}
	/**
	 * Method for getting only the most recently added assessments rather than
	 * all of the assessments currently held.
	 * @return
	 */
	public static ArrayList<Assessment> getNewAssessments(){
		return arryListNewStudentAssessments;
	}
	
	}
	

