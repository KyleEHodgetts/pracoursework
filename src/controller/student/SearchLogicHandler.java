package controller.student;

import java.util.ArrayList;

import model.student.Student;

/**
 * 
 * @author Kyle Hodgetts
 * @author Alex Gubbay
 * @author Peter Barta
 * @version 1.0
 * 
 * <p>Class that handles all the search logic that takes place in this application.</p>
 */
public class SearchLogicHandler {

	private ArrayList<Student> arryLstStudents;

	/**
	 * <p>
	 * The Search Logic Constructor.
	 * Retrieves list of all students to be searched
	 * </p>
	 */
	public SearchLogicHandler() {
		new StudentDataManager();
		arryLstStudents = StudentDataManager.getStudents();
	}
	
	

	/**
	 * Takes serach key as peramter and checks name and ID of students.
	 * returns those that match.
	 * @param searchInput	The search key
	 * @return	ArrayList of students matching search key
	 */
	public ArrayList<Student> getFilteredList(String searchInput) {
		//quicker to return the basic list than rebuild a new one, if the search term is empty.
		if(searchInput.equals("")){
			return arryLstStudents;
		}
		// arrayList of matching strings to be added to the new list.
		ArrayList<Student> filtered = new ArrayList<Student>();
		// searches the standard ArrayList finding matches and adding them to
		// the arrayList
		for (int i = 0; i < StudentDataManager.getRecordSize() ; i++) {
			if (arryLstStudents.get(i).getName().toLowerCase()
					.contains(searchInput.toLowerCase())||String.valueOf(arryLstStudents.get(i).getID()).contains(searchInput)) {
				Student x = arryLstStudents.get(i);
				filtered.add(x);
				

			}
			
		}
		// creates the arrayList to be returned and fills it with the arrayList
		// converted to an array.

		
		

		return filtered;

	}


	/**
	 * 
	 * @return The unfiltered list of students.
	 */
	public ArrayList<Student> getFilteredList() {

		return StudentDataManager.getStudents();

	}
}