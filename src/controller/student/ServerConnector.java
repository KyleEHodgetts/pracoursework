package controller.student;

import javax.swing.JOptionPane;

import studentdata.Connector;
import studentdata.DataTable;


/**
 * 
 * Class for opening connection to server and retrieving a copy of the Student data table.
 *
 */
public class ServerConnector
{
	/**
	 * Class for retrieving the student from server in the form of a DataTable.
	 * @return DataTable containing the student records retrieved from the server.
	 */
	public DataTable getStudentRecord(){
		{
			Connector server = new Connector();
	        boolean success = server.connect("Victorious_Secret","a2453474872e5402a76966dbeeb61e3c");
	        if (success == false) {
	           JOptionPane.showMessageDialog(null, "Fatal Error. Unable to connect to server","Connection Error", JOptionPane.ERROR_MESSAGE);
	           
	            System.exit(1);
	        }
	        
	        DataTable data = server.getData();
	       
	        return data;
	}
	}
	
	
	
}
