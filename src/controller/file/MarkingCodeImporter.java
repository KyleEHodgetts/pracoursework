package controller.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.student.StudentDataManager;
/**
 * Class for importing Marking codes from csv file and attaching them to the correct student object.
 * 
 * @author Alex Gubbay
 * @author Peter Barta
 * @author Kyle Hodgetts 
 * @version 1.0
 * 
 *
 */
public class MarkingCodeImporter {

	private ArrayList<Integer> arryLstStudentNumber;
	private ArrayList<String> arryLstannonMarkingCodes;
	private File csv;
	private String csvDirectory;
	private int failedImports;
	private int succImports;
	//private static boolean annonCodesAdded;
	
	/**
	 * Constructs the file browser window and sorts values into appropriate categories and assigns marking codes with appropriate student
	 */
	public MarkingCodeImporter() {
		/*
		 * creates the file selection window, and set selection path to the file
		 * selected by the user.
		 * Also instantiates the ArrayLists to contain the marking codes and
		 * student numbers.
		 */
				

		FileSelectionDialoges csvImport=new FileSelectionDialoges();
	try{
		csv = csvImport.getFile();
		if(csv!=null){
			csvDirectory=csv.getPath();
		
		arryLstannonMarkingCodes = new ArrayList<String>();
		arryLstStudentNumber = new ArrayList<Integer>();
		
		//once complete the constructor calls the ammendRecords method.
		
		ammendRecords();
		}
	}catch(Exception NullPointerExeption){
	//
		NullPointerExeption.printStackTrace();
		JOptionPane.showMessageDialog(csvImport, "No file was chosen", "File Picker", JOptionPane.ERROR_MESSAGE);
	}
		
	}

	private void ammendRecords() {
		
		try {
			
			//creates the buffered reader to read the information from the specified csv file.
			BufferedReader br = new BufferedReader(new FileReader(csvDirectory));
			String LineInput;
			//Marking codes have now been added.
			while ((LineInput=br.readLine()) != null) {
			
				String[] splitValues = LineInput.split(",");
				int studentNumber = Integer.valueOf(splitValues[0]);
				String annonCode = splitValues[1];
				arryLstannonMarkingCodes.add(annonCode);
				arryLstStudentNumber.add(studentNumber);

			}
			if (br.readLine() == null) {
				br.close();
			}
			//Marking codes have now been added.
		} catch (FileNotFoundException e) {

			JOptionPane.showMessageDialog(null, "The selected file could not be found", "Ipmort Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "The selected file could not be read", "Ipmort Error", JOptionPane.ERROR_MESSAGE);
			return;
		} catch(java.lang.NumberFormatException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "The Selected File Is of Invalid Format", "Ipmort Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		for (int i = 0; i < arryLstannonMarkingCodes.size(); i++) {
			//adds the anonymous marking codes the the student with the ID number given
			boolean fail = StudentDataManager.addAnnonCode(
					arryLstStudentNumber.get(i),
					arryLstannonMarkingCodes.get(i));
			
			if (fail == true) {
				failedImports++;
			} else {
				succImports++;
			}
		}
		return;
	}
		
	
	
	/**
	 * 
	 * @return The number of successful imports from when the .csv file was uploaded
	 */
	public int getSuccessfulImports(){
		return this.succImports;
	}
	
	/**
	 * 
	 * @return The number of failed imports from when the .csv file was uploaded
	 */
	public int getFailedImports(){
		return this.failedImports;
	}
}
