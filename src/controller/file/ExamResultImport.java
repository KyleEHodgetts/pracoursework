package controller.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import controller.student.StudentDataManager;
import model.result.Assessment;
import model.result.Result;

/**
 * @author Alex Gubbay, Peter Barta, Kyle Hoddgets
 *Class for importing exam results from csv file
 */
public class ExamResultImport {

	private FileSelectionDialoges filePicker;
	private String marksDirectory;
	private ArrayList<Assessment> ImportedAssesments;

	public ExamResultImport() throws Exception {
		filePicker = new FileSelectionDialoges();

		File marks = filePicker.getFile();
		if(marks!=null){
		marksDirectory = marks.getPath();

		ImportedAssesments = new ArrayList<Assessment>();
		try {
			addResults(marksDirectory);
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
	}

	/**
	 * Reads results from given diectory and adds them as assessments to the ArrayList.
	 * @param marksDirectory string of directory of csv file.
	 * @throws IOException when file cannot be read.
	 */
	private void addResults(String marksDirectory) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(marksDirectory));

		String HeaderRow = br.readLine();

		String[] headerValues = HeaderRow.split(",");
		int moduleRow = 0;
		int assessmentRow = 0;
		int candKeyRow = 0;
		int markRow = 0;
		int gradeRow = 0;
		int nameRow=0;
		for (int i = 0; i < headerValues.length; i++) {
			String currentHeader = headerValues[i].replace("\"", "");;

			if (currentHeader.contains("#Module")) {
				moduleRow = i;
				
			} else if (currentHeader.contains("Ass")) {
				assessmentRow = i;
			} else if (currentHeader.contains("Cand Key")) {
				candKeyRow = i;
			} else if (currentHeader.contains("Mark")) {
				markRow = i;
			} else if (currentHeader.contains("Grade")) {
				gradeRow = i;
			}else if(currentHeader.contains("Name")){
				nameRow=i;
			}
		}

		String LineInput;
		while ((LineInput = br.readLine()) != null) {
			//takes the data from the .csv file and adds it to the correct variables
			String valuesQuotesRemoved=LineInput.replace("\"", "");
			String valuesHashremoved=valuesQuotesRemoved.replace("#", "");
			String[] splitValues = valuesHashremoved.split(",");
			String moduleCode = splitValues[moduleRow];
			String assessment = splitValues[assessmentRow];
			String candKey = splitValues[candKeyRow];
			String name= splitValues[nameRow];
			String newCandKey =candKey;
			//candidateKey removes the #
			if(candKey.startsWith("#")){
				newCandKey=candKey.substring(1);
			}
			//gets the mark value and converts it to string.
			int mark = Integer.parseInt(splitValues[markRow]);
			String grade = splitValues[gradeRow];
			Result tempResult = new Result(moduleCode, assessment, newCandKey,
					mark, grade);
			if(!name.equals("")){
				tempResult.setName(name);
			}

			// option 1: AssessmentList is empty
			if (ImportedAssesments.size() == 0) {
				ImportedAssesments.add(new Assessment());
				ImportedAssesments.get(0).addResult(tempResult);
			}
			// option 2: Find the right assessment entry and add result to it.
			else if (ImportedAssesments.size() > 0) {
				//if the assessment listed in the result is found, this will be set to true.
				boolean found=false;
				for (int i = 0; i < ImportedAssesments.size(); i++) {
					if (tempResult.getAssessment().equals(ImportedAssesments.get(i).getAssessmentCode())) {
						ImportedAssesments.get(i).addResult(tempResult);
						found=true;
					}
				}
				//option 3: No matching assessment was found create a new one and add the result to it.
				if (found==false) {

				ImportedAssesments.add(new Assessment());
				ImportedAssesments.get(ImportedAssesments.size() - 1)
						.addResult(tempResult);
				}
			}
			
			}
		
	
		
		//the filled arrayList is passed to the StudentDataManager.
		StudentDataManager.importAssessments(ImportedAssesments);
		br.close();
	}


}
