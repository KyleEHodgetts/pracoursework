package controller.file;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

@SuppressWarnings("serial")
/**
 * Class for managing displaying JFileChooser GUIs for opening and saving files, and returning the data
 * to whichever feature requests it.
 * @author Alex Gubbay
 *
 */
public class FileSelectionDialoges extends JFrame {

	private FileNameExtensionFilter pdfFilter;
	private FileNameExtensionFilter csvFilter;

	private JFileChooser saveDialoge;
	private JFileChooser openDialoge;

	public FileSelectionDialoges() {

	}

	
	public File getFile() throws Exception {

		openDialoge = new JFileChooser();

		csvFilter = new FileNameExtensionFilter(".csv Files", "csv");
		openDialoge.setDialogTitle("Choose .csv file to import from");
		openDialoge.setApproveButtonText("Import");
		openDialoge.setFileFilter(csvFilter);

		File csvFile = null;

		int openDialogeValue = openDialoge.showOpenDialog(this);
		if (openDialogeValue == JFileChooser.APPROVE_OPTION) {
			csvFile = openDialoge.getSelectedFile();
			return csvFile;
		} else {

			return null;
		}
	}

	public String getDirectory() {

		saveDialoge = new JFileChooser();

		pdfFilter = new FileNameExtensionFilter("PDF Document", "pdf");
		saveDialoge.addChoosableFileFilter(pdfFilter);

		String SavefilePath = null;

		int fileChosenValue = saveDialoge.showSaveDialog(this);
		if (fileChosenValue == JFileChooser.APPROVE_OPTION) {
			SavefilePath = saveDialoge.getSelectedFile().getAbsolutePath();
			return SavefilePath;
		} else {

			return null;
		}
	}

}
