package controller.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import model.result.Result;
import model.student.Student;
import model.student.keatsEntry;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 * Class for creating and saving student data reports as PDF documents.
 * Utilizes the PDFbox library licensed under the Apache License v2.0.
 * {@link https://pdfbox.apache.org/index.html}
 * @author Alex Gubbay
 *
 */
public class PDFprinter {

@SuppressWarnings("unused")
private ArrayList<Student> data;
private String timeOfCreation;
private PDDocument docToBeExported;
private PDFont fontTitle;
private PDFont fontMain;
private PDFont fontHeader;
private PDPage pageCover;
private PDRectangle pageRectangle;
private int cursorCurrentPosition;
private String FileName;
private float pageHeight;

/**
 * Creates the PDF printer objects which can be done at stage separate to the actual creation
 * of the PDF document to reduce wait time for the user.
 */
public PDFprinter(){
	//Do these at constructor time to reduce wait creating the PDF.
	fontTitle = PDType1Font.HELVETICA_BOLD;
	fontMain = PDType1Font.HELVETICA;
	fontHeader = PDType1Font.HELVETICA_OBLIQUE;
	pageCover = new PDPage(PDPage.PAGE_SIZE_A4);
	pageRectangle = pageCover.getMediaBox();
}
/**
 * 
 * @param data This is an ArrayList of <code> Student </code> objects to be printed
 * @param FileName name and path to save the PDF as.
 * @throws IOException thrown if there is a problem with the name or path of file user has
 * Selected, for example if the file already exists.
 * @throws COSVisitorException thrown by an internal error writing to the PDF file.
 */
	public void CreatePDF(ArrayList<Student> data, String FileName) throws IOException, COSVisitorException{
		this.FileName=FileName;

		//creates the file and collects required data.
		this.data=data;
		timeOfCreation = Calendar.getInstance().getTime().toString();
		docToBeExported =new PDDocument();
		this.pageHeight=pageRectangle.getHeight();
		cursorCurrentPosition=0;
		PDPageContentStream titleContentStream = new PDPageContentStream(docToBeExported, pageCover);
		
		//Creates the title of the cover page.
		titleContentStream.beginText();
		titleContentStream.setFont(fontTitle, 26);
		titleContentStream.moveTextPositionByAmount(35, pageHeight - 70*(++cursorCurrentPosition));
		titleContentStream.drawString("Student Data Print-Out Summary");
		titleContentStream.setFont(fontMain, 14);
		titleContentStream.endText();
		
		//Creates the second line of the cover page.
		titleContentStream.beginText();
		titleContentStream.moveTextPositionByAmount(35, pageHeight - 70*(++cursorCurrentPosition));
		titleContentStream.drawString("The records exported can be found on the following pages.");
		titleContentStream.endText();
		
		//Creates the third line of the cover page.
		titleContentStream.beginText();
		titleContentStream.moveTextPositionByAmount(35, pageHeight - 70*(++cursorCurrentPosition));
		titleContentStream.drawString("This report was created on "+timeOfCreation+" and contains "+data.size()+" entries.");
		titleContentStream.endText();
		titleContentStream.close();
		
		//Creates the forth line of the cover page.
		titleContentStream.beginText();
		titleContentStream.moveTextPositionByAmount(35, pageHeight - 70*(++cursorCurrentPosition));
		titleContentStream.drawString("Results and participation data will be shown, if available.");
		titleContentStream.endText();
		titleContentStream.close();
		
		docToBeExported.addPage(pageCover);

		//creates the pages from each Student entry in the ArrayList passed to the method.
		ArrayList<PDPage> pages=new ArrayList<PDPage>();
		
		
		for (int i = 0; i < data.size(); i++) {
			//creates the new page and sets up the size and "cursor' position.
			cursorCurrentPosition=2;
			PDPage tempPage = new PDPage(PDPage.PAGE_SIZE_A4);
			pages.add(tempPage);
			PDPageContentStream mainPagesCS= new PDPageContentStream(docToBeExported, pages.get(i));
			mainPagesCS.setFont(fontHeader, 12);
			
			//adds page header
			mainPagesCS.beginText();
			mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30);
			mainPagesCS.drawString("Student Data Export Page "+pages.size()+" At "+timeOfCreation);
			mainPagesCS.setFont(fontTitle, 20);
			cursorCurrentPosition++;
			mainPagesCS.endText();
			
			//adds name at top of page
			mainPagesCS.beginText();
			mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(cursorCurrentPosition));
			mainPagesCS.drawString(data.get(i).getName());
			mainPagesCS.setFont(fontMain, 12);
			mainPagesCS.endText();
			
				mainPagesCS.drawLine(70, (30*(cursorCurrentPosition)), 530, (30*(cursorCurrentPosition)));
				if(i<data.size()){
					
					mainPagesCS.beginText();
					mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(++cursorCurrentPosition));
					
					
					mainPagesCS.drawString("Student Record Summary for ID #"+data.get(i).getID());
					mainPagesCS.endText();
					
					//adds Tutor email and marking code, if held.
					mainPagesCS.beginText();
					mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(++cursorCurrentPosition));
					if(data.get(i).getAnnonMarkingCode().equals("")){
					mainPagesCS.drawString("Tutor contact:  "+data.get(i).getTutorEmail());
					}
					else if(!(data.get(i).getAnnonMarkingCode().equals(""))){
						mainPagesCS.drawString("Tutor contact:  "+data.get(i).getTutorEmail()+" Marking code held: "+data.get(i).getAnnonMarkingCode());
						}
					
					mainPagesCS.endText();
					
					//adds email
					mainPagesCS.beginText();
					mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(++cursorCurrentPosition));
					mainPagesCS.drawString("Contact e-mail: "+data.get(i).getEmail());
					mainPagesCS.setFont(fontTitle, 12);
					mainPagesCS.endText();
					mainPagesCS.drawLine(70, ((620)), 530, ((620)));
					cursorCurrentPosition++;
					
					//adds results header
					mainPagesCS.beginText();
					mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(++cursorCurrentPosition));
					mainPagesCS.drawString("Currently Known Exam Results:");
					mainPagesCS.setFont(fontMain, 12);
					mainPagesCS.endText();
					
					//adds results data
					ArrayList<Result> resultsToDisplay=data.get(i).getResults();
					for(int k=0; k<resultsToDisplay.size(); k++){
						mainPagesCS.beginText();
						mainPagesCS.setFont(fontMain, 12);
						mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(++cursorCurrentPosition));
						mainPagesCS.drawString(resultsToDisplay.get(k).toFormattedString());
						mainPagesCS.setFont(fontTitle, 12);
						mainPagesCS.endText();
						}
					ArrayList <keatsEntry> participationData=data.get(i).getParticipationData();
					
					//adds participation data header
					++cursorCurrentPosition;
					mainPagesCS.drawLine(70, ((500+(30*resultsToDisplay.size()))), 530, ((530)));
					mainPagesCS.beginText();
					mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(++cursorCurrentPosition));
					mainPagesCS.drawString("Currently Known Participation Data:");
					mainPagesCS.setFont(fontMain, 12);
					mainPagesCS.endText();
					
					//adds participation data
					for(int k=0; k<participationData.size(); k++){
						mainPagesCS.beginText();
						mainPagesCS.setFont(fontMain, 12);
						mainPagesCS.moveTextPositionByAmount(70, pageHeight - 30*(++cursorCurrentPosition));
						mainPagesCS.drawString("Module Code: "+participationData.get(k).getModule()+" Last log in: "+participationData.get(k).getLastLogin());
						mainPagesCS.setFont(fontTitle, 12);
						mainPagesCS.endText();
						}
					
				}else{
					break;
				}
				mainPagesCS.close();
			
			cursorCurrentPosition=2;
			docToBeExported.addPage(pages.get(i));
		}
			

			docToBeExported.save(this.FileName);
			docToBeExported.close();
		}
	}
