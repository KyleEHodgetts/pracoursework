package controller.main;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;


/**
 * The GUI component of the Loading bar that is displayed when the program 
 * is opened and the data is downloaded from the server.
 * 
 * Also initiates the thread that initiates <code> StudentDataManager </code> 
 * which is where the JProgressBar is updated from.
 * @author Alex Gubbay
 *
 */
@SuppressWarnings("serial")
public class OpenLoadBar extends JFrame{

	private JProgressBar dataImportBar;
	private JPanel jpMain;
	private Border basicBorder;
	private JLabel jlTitle;
	public OpenLoadBar(){
		
		setSize(300, 100); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		basicBorder=  new EmptyBorder(10,10,10,10);
		jpMain=new JPanel();
		jpMain.setBorder(basicBorder);
		jpMain.setLayout(new BoxLayout(jpMain, BoxLayout.Y_AXIS));
		
		
		jlTitle=new JLabel("Loading Student Data");
		jlTitle.setBorder(basicBorder);
		dataImportBar = new JProgressBar(0, 216);

	
		jpMain.add(jlTitle);
		jpMain.add(dataImportBar);
		
		
		dataImportBar.setBounds(10, 10, 220, 20);
		
		add(jpMain);
		setResizable(false);
		setVisible(true);
		
		//Sets the progressbar always on top and in the center of the screen.
		setAlwaysOnTop(true);
		setLocationRelativeTo(null);

			//thread that starts the download and updates the progress bar.
		new Thread(new ProgramOpenThread(dataImportBar, this)).start();
		}
	
	
	}
	
	

	
