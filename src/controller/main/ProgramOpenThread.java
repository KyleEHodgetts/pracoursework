package controller.main;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

import controller.student.StudentDataManager;
import view.main.MainInterface;
/**
 * Creates the separate thread for the download of data <code> StudentDataManager </code> class. 
 * This can be time consuming process so it is executed in a separate thread, and a progress
 * bar is displayed to the user to show that the program has not crashed. The main program cannot be opened
 * until this is done.
 * @author Alex Gubbay
 *
 */
public class ProgramOpenThread implements Runnable{
	
	//JProgress bar that is displayed to the user.
	private JProgressBar dataImportBar;
	//JFrame that the JProgress bar is displayed in.
	private JFrame loadWindow;
	
	/**
	 * Takes refrences to the GUI components so that it can update them as
	 * the thead progresses.
	 * 
	 * @param dataImportBar JProgress bar that is to be updated
	 * @param loadWindow	JFrame that contains the bar. 
	 */
	public ProgramOpenThread(JProgressBar dataImportBar, JFrame loadWindow){
		this.dataImportBar=dataImportBar;
		this.loadWindow=loadWindow;
	}

	/**
	 * Runs the StudentDataManager.initateRecordsManager(JProgressBar); method
	 * in a separate thread. The method will update the value of the ProgressBar that is
	 * passed to it as it runs.
	 * When complete, it will create a new <code> MainInterface </code> instance and 
	 * close the window containing the JProgressBar.
	 * @see MainInterface
	 * 
	 */
	@Override
	public void run() {
			
			StudentDataManager.initateRecordsManager(dataImportBar);
			
			MainInterface window = new MainInterface();
			window.setVisible(true);
			loadWindow.dispose();
			
	}
	
	
	}

