package controller.email;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @author Kyle Hodgetts
 * @version 1.0
 * <p>Reads and writes settings to and from the settings.ini file.</p>
 */
public class INIHandler {

	private Properties p;
	private FileInputStream input;
	private FileOutputStream output;
	private String serverName;
	private int portNum;
	private String userName;
	private String email;
	private String connecSec;

	/**
	 * <p>INI Handler constructor. Retrieves current INI settings</p>
	 * @see FileInputStream
	 * @see FileOutputStream
	 * @throws FileNotFoundException	When settings.ini does not exist.
	 * @throws IOException				If any other problems occur.
	 */
	public INIHandler() throws FileNotFoundException, IOException{
		try{
			this.p = new Properties();
			this.getSettings();
		}
		catch(Exception e){
			this.writeSettings("testemailkcl", "smtp.gmail.com", 584, "TLS");
		}
	}
	
	public void createFile() throws IOException{
		File ini = new File("settings.ini");
		ini.createNewFile();
		this.writeSettings("USERNAME HERE", "smtp.gmail.com", 587, "TLS");
	}

	private void getSettings() throws IOException{
		try{
			this.input = new FileInputStream("settings.ini");
			p.load(input);
			this.serverName = p.getProperty("ServerName");
			this.portNum = new Integer(p.getProperty("Port"));
			this.email = p.getProperty("Email");
			this.connecSec = p.getProperty("ConnecSec");
			input.close();
			String[] user = this.email.split("@");
			this.userName = user[0];
		}
		catch(FileNotFoundException fnf){
			createFile();
		}
		

	}

	/**
	 * <p>Writes {@aUsername}, {@aServerName}, {@aPortNumber} to settings.ini to be used when sending emails
	 * @param aUserName					A username, which will have @gmail.com appending to it for sending emails
	 * @param aServerName				The choice of SMTP server. By default it is {@smtp.gmail.com}
	 * @param aPortNumber				The choice of port. By default it is {@584}
	 * @throws FileNotFoundException	When {@settings.ini} does not exist.
	 * @throws IOException				If any other problems occur.
	 */
	public void writeSettings(String aUserName, String aServerName, int aPortNumber, String aConnecSec) throws FileNotFoundException, IOException{
		this.input = new FileInputStream("settings.ini");
		this.output = new FileOutputStream("settings.ini");
		p.load(input);
		String[] emailEnd = aServerName.split("\\."); //Gets the end of the email signature e.g. @email.com
		p.put("Email", aUserName+"@"+emailEnd[1]+"."+emailEnd[2]);
		p.put("Username", aUserName);
		p.put("ServerName", aServerName);
		p.put("Port", Integer.toString(aPortNumber));
		p.put("ConnecSec", aConnecSec);
		p.store(output, null);
		output.close();
		input.close();
		this.getSettings();
	}

	/**
	 * 
	 * @return The current server being used. Null if one hasn't been assigned
	 */
	public String getServerName(){
		return this.serverName;
	}

	/**
	 * 
	 * @return the chosen port number. Null if one hasn't been assigned
	 */
	public int getPortNumber(){
		return this.portNum;
	}

	/**
	 * 
	 * @return The chosen username. Null if one hasn't been assigned
	 */
	public String getUsername(){
		return this.userName;
	}

	/**
	 * <p>Email which the mail will be sent from</p>
	 * @return {@getUsername + "@gmail.com"}
	 */
	public String getEmail(){
		return this.email;
	}

	/**
	 * <p>The choices for this are <code>TSL</code> and <code>SSL</code></p>
	 * @return the connection security method that will be used to send emails
	 */
	public String getConnecSec(){
		return this.connecSec;
	}
}
