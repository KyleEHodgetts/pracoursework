package controller.email;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import model.result.Result;
import model.student.Student;

/**
 * Generates email properties and email content on construction.
 * Sends emails to mailing list when <code>sendEmails()</code> is invoked.
 * @author Kyle Hodgetts
 * @version 1.0
 * @see {@link http://www.oracle.com/technetwork/java/javamail/index-141777.html}
 */
public class EmailSender extends SwingWorker<Boolean, Integer>{

	private String headerText;
	private HashMap<Integer,Student>emailList;
	private StringBuffer bodyText;
	private String footerText;
	private String password;
	private String from;
	private String host;
	private Properties props;
	private INIHandler details;
	private ArrayList<Student> students;

	private JProgressBar bar;
	private int percentComplete;
	private boolean success;

	/**
	 * Constructs an Email Sender
	 * @param headerText
			The text to appear before the module results
	 * @param emailList
			The list of students to be emailed
	 * @param footerText
			The text to appear after the module results
	 * @param password
			The password for the email account for authentications.
	 * @throws FileNotFoundException
			When the ini file is not found
	 * @throws IOException
			When one or more emails fail to send
	 */
	public EmailSender(String headerText, HashMap<Integer,Student>emailList, String footerText, String password, JProgressBar bar) throws FileNotFoundException, IOException{
		this.headerText = headerText;
		this.emailList = emailList;
		this.footerText = footerText;
		this.password = password;
		this.bar = bar;
		init();
	}


	/*
	 * Initialises Email sender
	 */
	private void init() throws FileNotFoundException, IOException{
		bar.setValue(0);	//Set progress bar to 0%
		details = new INIHandler();
		students = new ArrayList<Student>(emailList.values());	//List of students that are to be emailed
		
		// Sender's email ID
		this.from = details.getEmail();

		// Assuming you are sending email through relay.jangosmtp.net
		this.host = details.getServerName();

		/*
		 * Set properties for sending emails 
		 */
		props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", details.getPortNumber());

		/*
		 * Increments the progress bar as each email is sent.
		 */
		this.addPropertyChangeListener(new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				switch(evt.getPropertyName()){
				case "progress":
					bar.setValue((Integer)evt.getNewValue());
				}
			}

		});
	}

	/**
	 * Executes the email sending process.
	 * Sends an email to all students in the <code>emailList</code>
	 * @see Session
	 * @see javax.mail.Authenticator
	 */
	@Override
	protected Boolean doInBackground() throws Exception {

		try{
			// Get the Session object.
			Session session = Session.getInstance(props,
					new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(from, password);
				}
			});

			percentComplete = (students.size() / 100);	//Value of 1%

			for(int i = 0; i<students.size(); ++i){

				// Create a default MimeMessage object.
				Message message = new MimeMessage(session);
				
				// Set From: Email saved in the email settings file
				message.setFrom(new InternetAddress(from));
				
				// Set To: Student
				// Set CC: Student's Personal Tutor
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(students.get(i).getEmail()));
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(students.get(i).getTutorEmail()));

				// Set Subject: header field
				message.setSubject("Assessment Results");

				bodyText = new StringBuffer();

				ArrayList<Result>studentResults = students.get(i).getResults();
				for(int j = 0; j<studentResults.size(); ++j){
					bodyText.append(studentResults.get(j).toFormattedString()+"\n");
				}

				// Now set the actual message
				message.setText(headerText + "\n" + bodyText.toString() + "\n\n" + footerText);

				// Send message
				Transport.send(message);
				publish(++percentComplete);
			}

		}
		catch(AuthenticationFailedException e){
			success = false;
			return false;
		}
		success = true;
		return true;
	}


	/**
	 * Sets the state of the progress bar once the 
	 * email sending process is complete.
	 */
	@Override
	protected void done() {
		bar.setValue(students.size());
	}

	/**
	 * Updates progress bar after every email sent
	 */
	@Override
	protected void process(List<Integer> chunks) {
		bar.setValue(chunks.get(chunks.size()-1));
	}

	/**
	 * 
	 * @return the size of the email list
	 */
	public int getEmailListSize() {
		return students.size();
	}

	/**
	 * 
	 * @return true if all email's were sent successfully
	 * 			false otherwise
	 */
	public boolean wasSuccess(){
		return success;
	}

}
