package view.pdf;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import model.student.Student;

import org.apache.pdfbox.exceptions.COSVisitorException;

import controller.file.FileSelectionDialoges;
import controller.file.PDFprinter;

@SuppressWarnings("serial")

/**
 * Class for creating second GUI page of the PDF export tool
 *@author Alex Guubay
 */
public class PDFexporterWizzardPage2 extends JFrame {

	private JPanel jpSurroundingBox;
	private JPanel jpFilePath;
	private JPanel jpstudentSelection;
	private String strSelectedDirectory;
	private JLabel jlfilePathInfo;
	private HashMap<Integer, Student> hmDataFromCheckWindow;
	private ArrayList<Student> arylstDataToExport;
	private JPanel 	jpFilePathDisplay;
	private JPanel jpFileSizeDisplay;
	private JLabel jlSelectedFilePath;
	private Border bStandardBorder;
	private Border bLowerBorder;
	private JButton jbExport; //member variable to allow access in ActionLister inner class.


	/**
	 * Creates the second stage in the PDF exporter wizard, prompting user for file name and save location,
	 * as well as giving information about the estimated file size.
	 * 
	 * @param parentWindow Previous window for the wizzard page to center it's self on.
	 * @param dataToExport HashMap of students to be included in the PDF export.
	 * @see PDFprinter
	 * @see PDFcheckWindow
	 */
	public PDFexporterWizzardPage2(Component parentWindow, HashMap<Integer, Student> dataToExport ){
		bStandardBorder=new EmptyBorder(20,20,20,20);
		bLowerBorder =new EmptyBorder(5, 20, 20, 20);

		this.hmDataFromCheckWindow=dataToExport;
		setTitle("Export Student Data To PDF");
		setLocationRelativeTo(parentWindow);
		jpSurroundingBox=new JPanel();
		jpSurroundingBox.setLayout(new BoxLayout(jpSurroundingBox, BoxLayout.Y_AXIS));
		jpSurroundingBox.setBorder(bLowerBorder);

		jpFilePathDisplay=new JPanel();
		jpFilePathDisplay.setLayout(new BoxLayout(jpFilePathDisplay, BoxLayout.X_AXIS));
		jpFilePathDisplay.setBorder(bLowerBorder);
		jlfilePathInfo=new JLabel("Choose File Directory...");
		jpFilePathDisplay.add(jlfilePathInfo);		

		jpSurroundingBox.add(jpFilePathDisplay);
		arylstDataToExport = new ArrayList<Student>(hmDataFromCheckWindow.values());

		jpFilePath=new JPanel();
		jpFilePath.setLayout(new BoxLayout(jpFilePath, BoxLayout.X_AXIS));

		JButton jbSelect=new JButton("Select");
		jbExport=new JButton("Export");
		JButton jbCancel=new JButton("Cancel");
		
		jbExport.setEnabled(false);
		jlSelectedFilePath=new JLabel("No File Directory Selected");
		jbSelect.addActionListener(new ActionListener() {

			
			
			@Override
			/**
			 * Creates the window that prompts user to select save location and file name.
			 * Adds 'pdf' to the end of the file name.
			 */
			public void actionPerformed(ActionEvent e) {
				FileSelectionDialoges file =	new FileSelectionDialoges();
				strSelectedDirectory =file.getDirectory();
				if(strSelectedDirectory!=null){
					strSelectedDirectory=strSelectedDirectory+".pdf";
					jlSelectedFilePath.setText(strSelectedDirectory);
					jbExport.setEnabled(true);
					pack();
				}
			}
		});

		jpFilePath.add(jbSelect);
		jpFilePath.add(jlSelectedFilePath);
		jpstudentSelection=new JPanel();
		

		jbCancel.addActionListener(new ActionListener() {

			/**
			 * Cancels the process and closes the window if "Cancel" is pressed by user.
			 */
			@Override
			public void actionPerformed(ActionEvent e) {

				dispose();
			}
		});
		
		jbExport.addActionListener(new ActionListener() {

			
			/**
			 * Initiates the PDF printing process taking the data supplied by the user in the previous steps.
			 * Creates a new <code> PDFprinter </code> object and creates passes the data it requires. 
			 * Also includes error handling for issues that may arise during the PDF creation process.
			 * 
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				PDFprinter pdfPrint=new PDFprinter();
				if(strSelectedDirectory!=null){
					try {
						pdfPrint.CreatePDF(arylstDataToExport, strSelectedDirectory);
						//Success message. Everything went OK!
						JOptionPane.showMessageDialog(null, "PDF Export complete.\nFile saved to "+strSelectedDirectory, "Export Complete", JOptionPane.INFORMATION_MESSAGE);
						setVisible(false);
						
					} catch (COSVisitorException e1) {
						//there was an error visiting the file.
						JOptionPane.showMessageDialog(null, "Error Writing To File \n Check access permissions.", "File Write Error", JOptionPane.ERROR_MESSAGE);

					} catch (IOException e1) {
						//Error in the file creation process. May be permissions aren't granted, file name is not valid or file already exists.
						JOptionPane.showMessageDialog(null, "Error Writing To File \n Check file name is Valid and doesnt already exist.", "File Write Error", JOptionPane.ERROR_MESSAGE);
					} finally{
						
					}
				}else{
					/* User pressed export without choosing a file. Shouldn't happen as export button wont be enabled until a file is
					 * chosen but just in case.
					 */
					JOptionPane.showMessageDialog(null, "No file has been chosen for export", "Export Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		jpstudentSelection.setLayout(new BoxLayout(jpstudentSelection, BoxLayout.X_AXIS));
		jpstudentSelection.setBorder(bStandardBorder);

		jpstudentSelection.add(jbCancel);
		jpstudentSelection.add(jbExport);

		jpFileSizeDisplay=new JPanel();
		jpFileSizeDisplay.setLayout(new BoxLayout(jpFileSizeDisplay, BoxLayout.X_AXIS));
		jpFileSizeDisplay.setBorder(new EmptyBorder(5,5,5,5));
		JLabel jlFileSize=new JLabel("Estimated File Size: "+0.85*arylstDataToExport.size()+"kb");
		String fSystemFont =jlFileSize.getFont().getFontName();

		Font fSystemFontItalic=new Font(fSystemFont, Font.ITALIC,12);
		jlFileSize.setFont(fSystemFontItalic);
		jpFileSizeDisplay.add(jlFileSize);

		jpSurroundingBox.add(jpFilePath);
		jpSurroundingBox.add(jpstudentSelection);
		jpSurroundingBox.add(jpFileSizeDisplay);
		add(jpSurroundingBox);
		pack();

	}

}
