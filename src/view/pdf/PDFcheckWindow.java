package view.pdf;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.student.Student;
import controller.student.SearchLogicHandler;


/**
 * @author Alex Gubbay
 * @author Kyle Hodgetts
 * @version 1.1
 * <p>Displays the interface which allows a user to export a<code>Student</code>'s data in PDF format</p>
 */
@SuppressWarnings("serial")
public class PDFcheckWindow extends JFrame implements ActionListener, TableModelListener{

	private Border border;						//Used to space out components
	private SearchLogicHandler searchLogic;		//Used to retrieve all students which will populate Table
	private JPanel studentPanel;				//Contains all components that are involved with the student list
	private JPanel buttonPanel;					//Contains the navigation buttons
	private JPanel navPanel;					

	private JButton btnSelectAll;				//Selects all students in the student list
	private JButton btnSelectNone;				//Deselects all checked students
	private JButton btnNext;					//Opens the next window in the process -PDFexporterWindow
	private JButton btnCancel;					//Closes the window

	private JScrollPane scrStudents;			//Contains the table of students
	private JTable tblStudents;					//Table of students



	private DefaultTableModel model;
	private HashMap<Integer, Student>exportSelectionList;

	/**
	 * <p>Constructs the Interface</p>
	 * @since version 1.0
	 */
	public PDFcheckWindow(){
		super("Export Data To PDF");
		this.createComponents();
		this.setActionListeners();
	}

	private void createComponents(){
		this.setLayout(new BorderLayout());
		this.border = new EmptyBorder(10,10,10,10);

		/*
		 * Sets the layouts for all the panels
		 */
		studentPanel = new JPanel();
		studentPanel = new JPanel();
		buttonPanel = new JPanel(new FlowLayout());
		navPanel = new JPanel(new FlowLayout());

		/*
		 * Instantiates buttons
		 * add action listeners
		 * adds them to the frame.
		 */

		btnSelectAll = new JButton("Select All");
		btnSelectNone = new JButton("Select None");
		btnSelectAll.addActionListener(this);
		btnSelectNone.addActionListener(this);
		buttonPanel.add(btnSelectAll);
		buttonPanel.add(btnSelectNone);



		/*
		 * add button panel to the north region of the student panel
		 */
		studentPanel.setLayout(new BorderLayout());
		studentPanel.add(buttonPanel, BorderLayout.NORTH);

		/*
		 * Adds the border to space out the panel from others
		 */
		studentPanel.setBorder(new CompoundBorder(studentPanel.getBorder(), border));

		/*
		 * Add the labels and the textfields to their respective panels
		 * Add border to space out panel
		 */

		studentPanel.setLayout(new BorderLayout());
		studentPanel.add(buttonPanel, BorderLayout.NORTH);

		studentPanel.setBorder(new CompoundBorder(studentPanel.getBorder(), border));


		/*
		 * Add text panels to containing email panel
		 */
		btnCancel = new JButton("Cancel");
		btnNext = new JButton("Next");
		navPanel.add(btnCancel);
		navPanel.add(btnNext);


		/*
		 * Instantiate logic handler to retrieve students
		 */
		searchLogic = new SearchLogicHandler();
		loadStudents();

		/*
		 * Add all components to main frame
		 */
		this.add(studentPanel, BorderLayout.WEST);
		searchLogic = new SearchLogicHandler();
		loadStudents();


		this.add(studentPanel, BorderLayout.WEST);
		this.add(navPanel, BorderLayout.SOUTH);
		this.pack();
	}

	private void setActionListeners(){
		btnCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose(); //Close window
			}

		});

		btnNext.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				//no students selected
				if(exportSelectionList.isEmpty()){
					JOptionPane.showMessageDialog(null, "No students have been selected.", "Export Error", JOptionPane.WARNING_MESSAGE);

				}else{
					PDFexporterWizzardPage2 nextPane=new PDFexporterWizzardPage2(studentPanel,exportSelectionList);
					nextPane.setVisible(true);
					dispose();
					pack();
				}
			}

		});


	}

	private void loadStudents(){		
		ArrayList<Student> newList = searchLogic.getFilteredList();
		exportSelectionList = new HashMap<Integer,Student>();
		String[] colNames = {"Student", "Add to list"};
		Object[][] students = new Object[newList.size()][2];
		for(int i = 0; i<newList.size(); i++){
			students[i][0] = newList.get(i);
			students[i][1] = new Boolean(false);
		}

		/*
		 * Table model to handle Table data
		 */

		model = new DefaultTableModel(students, colNames);
		tblStudents = new JTable(model){
			@Override
			public Class getColumnClass(int column) {
				switch(column){
				case 0:
					return Student.class;
				default:
					return Boolean.class;
				}
			}
		};
		tblStudents.getModel().addTableModelListener(this);
		scrStudents = new JScrollPane(tblStudents);
		studentPanel.add(scrStudents, BorderLayout.CENTER);
	}



	/**
	 * <p>Action listener that (un)checks all checkboxes next to student objects and removes them from the export list</p>
	 * @param e ActionEvent		Will be either the Select All button or Select none button
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton)e.getSource();

		if(source.getText().equals("Select All")){
			for(int i = 0; i<tblStudents.getRowCount(); i++){
				tblStudents.setValueAt(new Boolean(true), i, 1); 	//Check all Checkboxes
				tblStudents.setValueAt(new Boolean(true), i, 1);
			}
		}
		else{
			for(int i = 0; i<tblStudents.getRowCount(); i++){
				tblStudents.setValueAt(new Boolean(false), i, 1);	//Likewise, uncheck them
				tblStudents.setValueAt(new Boolean(false), i, 1);
			}
		}
	}

	/**
	 * <p>Called whenever a a checkbox is checked or unchecked, also called when the select all and select none buttons are pressed</p>
	 * @param arg0 	A TableModelEvent
	 */
	@Override
	public void tableChanged(TableModelEvent arg0) {
		int row = arg0.getFirstRow();								//Get the first row that was affected
		int column = arg0.getColumn();								//Get the column
		TableModel model = (TableModel)arg0.getSource();			//Cast the table model to access methods
		Student data = (Student)model.getValueAt(row, column-1);	//Get the value at the row column intersection (Student)

		/*
		 * If the student is already on the mailing list,
		 * the checkbox was unchecked, therefore remove
		 * the student from the mailing list
		 */
		if(exportSelectionList.containsKey(row)){								
			exportSelectionList.remove(row);
		}
		/*
		 * Checkbox was checked, add student to mailing list
		 */
		if(exportSelectionList.containsKey(row)){
			exportSelectionList.remove(row);
		}

		else{
			exportSelectionList.put(row, data);
		}

	}

}
