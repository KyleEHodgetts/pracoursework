package view.email;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.result.Result;
import model.student.Student;
import controller.email.EmailSender;
import controller.student.SearchLogicHandler;


/**
 * 
 * @author Kyle Hodgetts
 * @version 1.1
 * <p>Email Interface that allows the user to pick and choose students to send modules marks to via Email.</p>
 */
@SuppressWarnings("serial")
public class EmailInterface extends JFrame implements ActionListener, TableModelListener{
	private Border border;	//Used to space out components
	private SearchLogicHandler searchLogic;	//Used to retrieve all students which will populate Table

	private JPanel studentPanel;	//Contains all components that are involved with the student list
	private JPanel emailPanel;
	private JPanel sendEmailPanel;
	private JPanel headerTextPanel;	//User will write text to come before the Student marks here
	private JPanel footerTextPanel;	//and after here.
	private JPanel buttonPanel;	//Contains the navigation buttons
	private JPanel navPanel;

	private JButton btnSelectAll;	//Selects all students in the student list
	private JButton btnSelectNone;	//Deselects all checked students
	private JButton btnNext;	//Opens email panel which the user can then send the email from
	private JButton btnCancel;	//Closes the window

	private JScrollPane scrStudents;	//Contains the table of students
	private JTable tblStudents;	//Table of students

	private EmailPanel ep;
	private JPanel sendButtonPanel;
	private JButton btnPrev;
	private JButton btnSend;
	private JLabel lblStatus;
	private JProgressBar pbEmail;
	private static final JLabel lblHeader = new JLabel("Header: ");
	private static final JLabel lblFooter = new JLabel("Footer: ");
	private JTextArea txtHeader;
	private JTextArea txtFooter;

	private DefaultTableModel model;
	private HashMap<Integer, Student>emailList;
	private EmailSender es;

	/**
	 * <p>Constructs the Interface</p>
	 * @since version 1.0
	 */
	public EmailInterface(){
		super("Email to Students");
		this.createComponents();
		this.setActionListeners();
	}
	private void createComponents(){
		this.setLayout(new BorderLayout());
		this.border = new EmptyBorder(10,10,10,10);
		/*
		 * Sets the layouts for all the panels
		 */
		studentPanel = new JPanel();
		emailPanel = new JPanel(new GridLayout(2,1));
		sendEmailPanel = new JPanel(new BorderLayout());
		studentPanel = new JPanel();
		emailPanel = new JPanel(new GridLayout(2,1));
		headerTextPanel = new JPanel(new FlowLayout());
		footerTextPanel = new JPanel(new FlowLayout());
		buttonPanel = new JPanel(new FlowLayout());
		navPanel = new JPanel(new FlowLayout());
		/*
		 * Instantiates buttons
		 * add action listeners
		 * adds them to the frame.
		 */

		btnSelectAll = new JButton("Select All");
		btnSelectNone = new JButton("Select None");
		btnSelectAll.addActionListener(this);
		btnSelectNone.addActionListener(this);
		buttonPanel.add(btnSelectAll);
		buttonPanel.add(btnSelectNone);
		txtHeader = new JTextArea(20,30);
		txtHeader.setLineWrap(true);
		txtFooter = new JTextArea(20,30);
		txtFooter.setLineWrap(true);

		/*
		 * add button panel to the north region of the student panel
		 */
		studentPanel.setLayout(new BorderLayout());
		studentPanel.add(buttonPanel, BorderLayout.NORTH);

		/*
		 * Adds the border to space out the panel from others
		 */
		studentPanel.setBorder(new CompoundBorder(studentPanel.getBorder(), border));

		/*
		 * Add the labels and the textfields to their respective panels
		 * Add border to space out panel
		 */
		studentPanel.setLayout(new BorderLayout());
		studentPanel.add(buttonPanel, BorderLayout.NORTH);

		studentPanel.setBorder(new CompoundBorder(studentPanel.getBorder(), border));

		/*
		 * Add header and footer panel components and set their borders to evenly
		 * seperate them
		 */
		headerTextPanel.add(lblHeader); headerTextPanel.add(new JScrollPane(txtHeader));
		footerTextPanel.add(lblFooter); footerTextPanel.add(new JScrollPane(txtFooter));
		headerTextPanel.setBorder(new CompoundBorder(emailPanel.getBorder(), border));
		footerTextPanel.setBorder(new CompoundBorder(emailPanel.getBorder(), border));


		/*
		 * Add text panels to containing email panel
		 */
		emailPanel.add(headerTextPanel);
		emailPanel.add(footerTextPanel);
		btnCancel = new JButton("Cancel");
		btnNext = new JButton("Next");
		navPanel.add(btnCancel);
		navPanel.add(btnNext);
		btnPrev = new JButton("Prev");
		btnSend = new JButton("Send");
		lblStatus = new JLabel("Idle: ");
		pbEmail = new JProgressBar();

		/*
		 * Instantiate logic handler to retrieve students
		 */
		searchLogic = new SearchLogicHandler();
		loadStudents();
		/*
		 * Add all components to main frame
		 */
		this.add(studentPanel, BorderLayout.WEST);
		this.add(sendEmailPanel, BorderLayout.CENTER);
		searchLogic = new SearchLogicHandler();
		loadStudents();
		this.add(studentPanel, BorderLayout.WEST);
		this.add(emailPanel, BorderLayout.CENTER);
		this.add(navPanel, BorderLayout.SOUTH);
		this.pack();
	}

	private void setActionListeners(){
		btnCancel.addActionListener(new ActionListener(){

			/**
			 * Closes the window
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose(); //Close window
			}
		});

		btnNext.addActionListener(new ActionListener(){

			/**
			 * Instantiates a new email panel with the 
			 * header and footer text added to the email preview
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					if(emailList.size() == 0){ throw new IndexOutOfBoundsException();}
					ep = new EmailPanel(txtHeader.getText(), txtFooter.getText());
					sendButtonPanel = new JPanel();
					sendButtonPanel.add(btnPrev);
					sendButtonPanel.add(btnSend);
					pbEmail.setStringPainted(true);
					sendButtonPanel.add(lblStatus);
					sendButtonPanel.add(pbEmail);
					ep.revalidate();
					remove(emailPanel);
					remove(navPanel);
					sendEmailPanel.add(ep, BorderLayout.CENTER);
					add(sendEmailPanel, BorderLayout.CENTER);
					add(sendButtonPanel, BorderLayout.SOUTH);
					pack();
				}
				catch(IndexOutOfBoundsException obe){
					JOptionPane.showMessageDialog(null, "You didn't select anybody", "Empty Mail List", JOptionPane.PLAIN_MESSAGE);

				}

			}
		});

		btnPrev.addActionListener(new ActionListener(){

			/**
			 * Returns to the previous view
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				remove(sendEmailPanel);
				remove(sendButtonPanel);
				add(emailPanel, BorderLayout.CENTER);
				add(navPanel, BorderLayout.SOUTH);
				repaint();
				pack();
			}
		});

		btnSend.addActionListener(new ActionListener(){

			/**
			 * Commences email sending process
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(ep.getPassword().length() == 0){throw new Exception();}
					if(emailList.size() == 0){}
					es = new EmailSender(txtHeader.getText(), emailList, txtFooter.getText(), ep.getPassword(), pbEmail);
					pbEmail.setMinimum(0);
					pbEmail.setMaximum(es.getEmailListSize());
					lblStatus.setText("Sending...");
					es.execute();

				}catch (Exception e){
					JOptionPane.showMessageDialog(null, "Email sending failed. \n Please check your password and email settings then try again", "Unsucessful", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		pbEmail.addChangeListener(new ProgressChangeListener());
	}


	private void loadStudents(){
		ArrayList<Student> newList = searchLogic.getFilteredList();
		emailList = new HashMap<Integer,Student>();
		String[] colNames = {"Student", "Add to list"};
		Object[][] students = new Object[newList.size()][2];
		for(int i = 0; i<newList.size(); i++){
			students[i][0] = newList.get(i);
			students[i][1] = new Boolean(false);
		}

		/*
		 * Table model to handle Table data
		 */
		model = new DefaultTableModel(students, colNames);
		tblStudents = new JTable(model){
			@Override
			public Class getColumnClass(int column) {
				switch(column){
				case 0:
					return Student.class;
				default:
					return Boolean.class;
				}
			}
		};

		/*
		 * Set table model for student table and add it to scrollpane and student panel
		 */
		tblStudents.getModel().addTableModelListener(this);
		scrStudents = new JScrollPane(tblStudents);
		studentPanel.add(scrStudents, BorderLayout.CENTER);
	}

	/**
	 * <p>Action listener that (un)checks all checkboxes next to student objects and removes them from the mailing list</p>
	 * @param e ActionEvent Will be either the Select All button or Select none button
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton)e.getSource();
		if(source.getText().equals("Select All")){
			for(int i = 0; i<tblStudents.getRowCount(); i++){
				tblStudents.setValueAt(new Boolean(true), i, 1); //Check all Checkboxes
				tblStudents.setValueAt(new Boolean(true), i, 1);
			}
		}
		else{
			for(int i = 0; i<tblStudents.getRowCount(); i++){
				tblStudents.setValueAt(new Boolean(false), i, 1);
				//Likewise, uncheck them
				tblStudents.setValueAt(new Boolean(false), i, 1);
			}
		}
	}

	/**
	 * <p>Called whenever a a checkbox is checked or unchecked, also called when the select all and select none buttons are pressed</p>
	 * @param arg0 A TableModelEvent triggered from checking or unchecking 1 or more boxes
	 */
	@Override
	public void tableChanged(TableModelEvent arg0) {
		int row = arg0.getFirstRow();	//Get the first row that was affected
		int column = arg0.getColumn();	//Get the column

		TableModel model = (TableModel)arg0.getSource();	//Cast the table model to access methods
		Student data = (Student)model.getValueAt(row, column-1);

		//Get the value at the row column intersection (Student)
		/*
		 * If the student is already on the mailing list,
		 * the checkbox was unchecked, therefore remove
		 * the student from the mailing list
		 */
		if(emailList.containsKey(row)){
			emailList.remove(row);
		}
		/*
		 * Checkbox was checked, add student to mailing list
		 */
		if(emailList.containsKey(row)){
			emailList.remove(row);
		}

		else{
			emailList.put(row, data);
		}

	}

	/**
	 * Displays the email panel that previews how the email will look to {@Student}'s once sent
	 * @author Kyle Hodgetts
	 *
	 */
	class EmailPanel extends JPanel {

		/*
		 * Email Panel components
		 */
		private String header;
		private String footer;
		private JTextArea emailText;
		private JPanel emailContainer;
		private JPanel passwordPanel;
		private JLabel lblPassword;
		private JPasswordField txtPassword;

		/**
		 * 
		 * @param header	Text that comes before the student marks
		 * @param footer	Text that comes after the student marks
		 */
		public EmailPanel(String header, String footer){
			this.header = header;
			this.footer = footer;
			this.setLayout(new BorderLayout());
			emailContainer = new JPanel();
			emailText = new JTextArea(50,80);
			generateEmail();
			emailContainer.add(emailText);
			this.passwordPanel = new JPanel(new FlowLayout());
			this.lblPassword = new JLabel("Password: ");
			this.txtPassword = new JPasswordField(25);
			this.passwordPanel.add(lblPassword);
			this.passwordPanel.add(txtPassword);
			this.add(emailContainer, BorderLayout.CENTER);
			this.add(passwordPanel, BorderLayout.SOUTH);
			this.setSize(50, 30);
		}
		private void generateEmail(){

			/*
			 * Generate template email for how the real one will look
			 * upone being sent
			 */
			emailText.append(this.header + "\n\n");		//Header text at top
			ArrayList<Student>students = new ArrayList<Student>(emailList.values());

			//Get the list of results of first student in maillist to generate sample email
			ArrayList<Result>sampleEmail = students.get(0).getResults();	
			for(Result r : sampleEmail){
				emailText.append(r.toFormattedString()+"\n");		//append each result into the email body
			}
			emailText.append(this.footer);	//Footer text at the bottom


		}

		/**
		 * 
		 * @return revealed password
		 */
		public String getPassword(){
			String pass = new String(this.txtPassword.getPassword());
			return pass;
		}
	}

	/**
	 * Closes Email Display once all emails have been sent.
	 * @author Kyle Hodgetts
	 *
	 */
	class ProgressChangeListener implements ChangeListener{

		/**
		 * 
		 * @param e Change event from JProgressBar
		 */
		@Override
		public void stateChanged(ChangeEvent e) {
			JProgressBar bar = (JProgressBar)e.getSource();
			if(bar.getValue() == emailList.size() && es.wasSuccess() == true){
				dispose();
			}
			else if(bar.getValue() == emailList.size() && es.wasSuccess() == false){
				lblStatus.setText("Idle: ");
				bar.setValue(0);
				JOptionPane.showMessageDialog(null, "Password incorrect, please try again", "Authentication Failure", JOptionPane.ERROR_MESSAGE);
			}
		}

	}
}