package view.email;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import controller.email.INIHandler;

/**
 * Displays the current email settings as read from the settings.ini file.
 * When OK is pressed, the current widget values are then written into the respective fields
 * in the ini file.
 * @author Kyle Hodgetts
 * @version 1.0
 * @see JFrame
 * @see ActionListener
 */
@SuppressWarnings("serial")
public class EmailSettingsDialogue extends JFrame implements ActionListener{
	private int defaultPort;

	private JPanel containerPanel;

	private JPanel settingsPanel;
	private JLabel lblServerName;
	private JTextField txtServerName;
	private JLabel lblPort;
	private JSpinner spinPort;
	private JLabel lblDefault;

	private JPanel secAuthPanel;
	private JLabel lblConnecSec;
	private JComboBox<String> cbConnecSec;
	private JLabel lblAuthMethod;
	private JComboBox<String> cbAuthMethod;
	private JLabel lblUsername;
	private JTextField txtUsername;

	private JPanel buttonPanel;
	private JButton btnCancel;
	private JButton btnOkay;

	private INIHandler ini;
	private String[] connecSecs = {"TLS", "SSL"};
	private String[] authMethods = {"Normal Password"};

	/**
	 * Constructs the email settings window.
	 */
	public EmailSettingsDialogue(){
		super("SMTP Server");
		defaultPort = 587;

		createComponents();
		setActionListeners();
		this.getEmailSettings();
	}

	private void createComponents(){

		containerPanel = new JPanel(new BorderLayout());

		/*
		 * Create settings panel and it's components
		 */
		settingsPanel = new JPanel(new GridLayout(3,2));
		lblServerName = new JLabel("Server Name: ");
		txtServerName = new JTextField(20);
		lblPort = new JLabel("Port: ");
		spinPort = new JSpinner();
		lblDefault = new JLabel("Default Port: "+defaultPort);

		/*
		 * Add all settings panel components to settings panel.
		 */
		settingsPanel.add(lblServerName);
		settingsPanel.add(txtServerName);
		settingsPanel.add(lblPort);
		settingsPanel.add(spinPort);
		settingsPanel.add(lblDefault);

		containerPanel.add(settingsPanel, BorderLayout.NORTH);	//Add settings panel to containing panel

		/*
		 * Create Security Authorisation panel and it's components
		 */
		secAuthPanel = new JPanel(new GridLayout(3,2));
		lblConnecSec = new JLabel("Connection Security");
		cbConnecSec = new JComboBox<String>(connecSecs);
		cbConnecSec.addActionListener(this);
		lblAuthMethod = new JLabel("Authentication Method: ");
		cbAuthMethod = new JComboBox<String>(authMethods);
		lblUsername = new JLabel("Username: ");
		txtUsername = new JTextField(25);

		/*
		 * Add all security authorisation components to the panel.
		 */
		secAuthPanel.add(lblConnecSec);
		secAuthPanel.add(cbConnecSec);
		secAuthPanel.add(lblAuthMethod);
		secAuthPanel.add(cbAuthMethod);
		secAuthPanel.add(lblUsername);
		secAuthPanel.add(txtUsername);

		containerPanel.add(secAuthPanel, BorderLayout.SOUTH);	//Add secAuth panel to containing panel

		this.add(containerPanel, BorderLayout.CENTER);

		/*
		 * Create button panel and it's components
		 */
		buttonPanel = new JPanel(new FlowLayout());
		btnCancel = new JButton("Cancel");
		btnOkay = new JButton("Ok");
		buttonPanel.add(btnCancel);
		buttonPanel.add(btnOkay);

		this.add(buttonPanel, BorderLayout.SOUTH);
		this.pack();
	}

	private void setActionListeners(){
		btnCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}

		});

		btnOkay.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					INIHandler ini = new INIHandler();
					ini.writeSettings(txtUsername.getText(), txtServerName.getText(), (int)spinPort.getValue(), (String)cbConnecSec.getSelectedItem());
					dispose();
				} catch (IOException e1) {
					repaint();
				}

			}

		});
	}

	private void getEmailSettings(){
		try {
			ini = new INIHandler();
			txtServerName.setText(ini.getServerName());
			spinPort.setValue(new Integer(ini.getPortNumber()));
			txtUsername.setText(ini.getUsername());
			cbConnecSec.setSelectedItem(ini.getConnecSec());
		} catch (NullPointerException | IOException npe){
			try {
				ini.createFile();
				txtServerName.setText(ini.getServerName());
				spinPort.setValue(new Integer(ini.getPortNumber()));
				txtUsername.setText(ini.getUsername());
				cbConnecSec.setSelectedItem(ini.getConnecSec());
			} catch (IOException e) {
				this.repaint();
			}
		}
	}

	/**
	 * Changes default port for server depending on connection method
	 * selected.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unchecked")
		JComboBox<String> cb = (JComboBox<String>)e.getSource();
		if(cb.getSelectedItem().equals("TLS")){
			spinPort.setValue(new Integer("587"));
		}
		else if (cb.getSelectedItem().equals("SSL")){
			spinPort.setValue(new Integer("465"));
		}
	}
}
