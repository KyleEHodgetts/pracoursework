package view.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;

import model.result.Assessment;
import model.student.Student;
import view.email.EmailInterface;
import view.email.EmailSettingsDialogue;
import view.pdf.PDFcheckWindow;
import view.student.ScatterplotInterface;
import view.student.SelectedStudent;
import view.student.fetchParticipationWindow;
import controller.file.ExamResultImport;
import controller.file.MarkingCodeImporter;
import controller.student.SearchLogicHandler;
import controller.student.StudentDataManager;

@SuppressWarnings("serial")
public class MainInterface extends JFrame{

	private JScrollPane scrollPane;
	private JList<Student> jlstStudentNamesList;
	private DefaultListModel<Student> listModel;
	private JTextField txtSearchForStudent;
	private SearchLogicHandler searchLogic;

	private JMenuBar mainBar;
	private JMenu fileMenu;
	private JMenu editMenu;
	private JMenu dataMenu;

	private JMenuItem compareToAverageItem;
	private JMenuItem loadExamResultsItem;
	private JMenuItem quitItem;
	private JMenuItem copyItem;
	private JMenuItem pasteItem;
	private JMenuItem deleteItem;
	private JMenuItem loadAnonymousMarkingCodesItem;
	private JMenuItem emailToStudentsItem;
	private JMenuItem emailSettingsItem;
	private JMenuItem scrapeKEATSitem;
	private fetchParticipationWindow fetchItem;
	private JMenuItem exportPDFitem;


	private JTabbedPane tabs;
	private DefaultTableModel model;
	private ArrayList<HashMap<Student, Double>>scatterplotData;
	private boolean hasRun;

	/**
	 * Create the application.
	 * @see JFrame
	 */
	public MainInterface() {
		super("Victorious Secret Prototype Window");
		initialize();
		this.setVisible(true);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		this.setResizable(false);
		this.setBounds(100, 100, 700, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		scatterplotData = new ArrayList<HashMap<Student, Double>>();

		JPanel searchPanel = new JPanel();
		searchPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, UIManager
				.getColor(Color.BLACK), UIManager.getColor(Color.BLACK),
				UIManager.getColor(Color.BLACK), UIManager
				.getColor(Color.BLACK)));
		searchPanel.setBackground(Color.WHITE);
		this.getContentPane().add(searchPanel, BorderLayout.WEST);

		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 0;
		gbc_list.gridy = 2;

		// creates list and populates it with initial items
		searchLogic = new SearchLogicHandler();
		jlstStudentNamesList = new JList<Student>();
		paintList();

		jlstStudentNamesList.addMouseListener(new MouseAdapter() { 
			//opens the selected student's information page upon double click.
			public void mousePressed(MouseEvent e) { 
				if (e.getClickCount() == 2) 
				{
					new SelectedStudent(jlstStudentNamesList.getSelectedValue())
					.setVisible(true);
				}
			} 
		}); 


		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.Y_AXIS));

		txtSearchForStudent = new JTextField();

		txtSearchForStudent.addFocusListener(new FocusListener()
		{
			//When clicked the textfield is clicked on the field clears, text darkens
			//and the list is automatically repainted (useful after initial search)
			@Override
			public void focusGained(FocusEvent fe)
			{
				txtSearchForStudent.setText("");
				txtSearchForStudent.setForeground(Color.BLACK);
				String search = txtSearchForStudent.getText();
				repaintList(search);
			}

			//Once the textfield is no longer selected, text will return to initial lightgray color. 
			@Override
			public void focusLost(FocusEvent fe)
			{
				txtSearchForStudent.setForeground(Color.LIGHT_GRAY);
				String search = txtSearchForStudent.getText();
				repaintList(search);
			}
		});


		txtSearchForStudent.addKeyListener(new KeyAdapter() {
			@Override
			// sub method for creating action from text input
			public void keyReleased(KeyEvent e) {
				String search = txtSearchForStudent.getText();
				repaintList(search);

			}
		});

		txtSearchForStudent.setText("Search for student by ID or name");
		txtSearchForStudent.setSize(new Dimension(150, 28));
		txtSearchForStudent.setMaximumSize(new Dimension(1000, 28));
		txtSearchForStudent.setBounds(new Rectangle(0, 0, 130, 20));
		searchPanel.add(txtSearchForStudent);
		txtSearchForStudent.setColumns(10);

		scrollPane = new JScrollPane();
		searchPanel.add(scrollPane);
		scrollPane.setViewportView(jlstStudentNamesList);

		fetchItem =new fetchParticipationWindow(this);
		createMenu(txtSearchForStudent, jlstStudentNamesList, this,  searchLogic);

	}	    


	protected void addTab(){

		//new Assessment list (collection of collections of results)
		if(tabs == null){
			tabs = new JTabbedPane();	
		}
		//gets the latest set of assessments.
		final ArrayList<Assessment> AssessmentList = StudentDataManager.getNewAssessments();
		//only creates tab if the dataset was non-empty.
		if (AssessmentList.isEmpty()==false) {
			
		
		for(int j=0; j<AssessmentList.size(); j++){
		HashMap<Student, Double>dataSet = new HashMap<Student,Double>(); //hashmap used for plotting students and marks on scatterplot
		String[] columnNames = {"Name", "Cand Key", "Mark","Grade"};
		JScrollPane tabScrollPane;
		//new TabbedPane created if none existing
		

		//Creates new table with uneditable cells and column headers
		JTable z = new JTable();
		model = new DefaultTableModel(columnNames,0){
			@Override
			public boolean isCellEditable(int row, int column) {
				//all cells false
				return false;
			}
		};

		Assessment last=AssessmentList.get(j);

		for (int i = 0; i < last.getNumberOfResults(); i++) {

			Vector<Object> data = new Vector<Object>();

			String name = last.get(i).getName();
			String candKey= last.get(i).getCandidateKey();
			String mark= String.valueOf(last.get(i).getMark());	
			String grade=last.get(i).getGrade();
			data.add(name);
			data.add(candKey);
			data.add(mark);
			data.add(grade);
			model.addRow(data); //adds name, cand key, mark, grade in row for each student.
			Student temp=StudentDataManager.getStudent(name);
			if(Integer.parseInt(mark) > 0){
				dataSet.put(temp, (double)last.get(i).getMark()); //adding student and mark data to hashmap for graph
			}
		}


		scatterplotData.add(dataSet);
		//student selection by matching name in column to student list
		z.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					JTable target = (JTable)e.getSource();
					int row = target.getSelectedRow();
					int column = target.getSelectedColumn();
					if(column == 0){
						String stuName = (String)target.getValueAt(row, column);
						Student toDisplay=StudentDataManager.getStudent(stuName);
						SelectedStudent x =new SelectedStudent(toDisplay);
						x.setVisible(true);
					}

				}
			}
		}); 

		z.setModel(model); //populates table with data for students

		tabScrollPane =  new JScrollPane(z);
		tabs.addTab(last.getModuleCode()+" "+last.getAssessmentCode(), tabScrollPane); //adds scrollpane with table and assessment name as tab title

		this.getContentPane().add(tabs, BorderLayout.CENTER);
		tabs.setVisible(false);

		}
		}
	}


	private void makeTabsVisible(){
		tabs.setVisible(true);

	}

	//method to add mouselistener to tabbedpane(only once)
	private void addTabClose(){
		tabs.addMouseListener(new MouseAdapter() {

			//close right clicked tab
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)){
					final int index = tabs.getUI ().tabForCoordinate ( tabs, e.getX (), e.getY () ); //reports number of tab clicked on
					if ( index != -1 ) //checking click is actually on a tab
					{

						tabs.removeTabAt ( index );

						repaint();
					}
				}
			}});
		hasRun = true; //will only execute once
	}



	private void paintList() {
		ArrayList<Student> newList = searchLogic.getFilteredList();
		// method for refreshing list after text input
		listModel = new DefaultListModel<Student>();

		for (int i = 0; i < newList.size(); i++) {
			listModel.addElement(newList.get(i));
		}
		jlstStudentNamesList.setModel(listModel);
		jlstStudentNamesList.setVisible(true);
	}

	private void repaintList(String searchInput) {
		ArrayList<Student> newList = searchLogic.getFilteredList(searchInput);
		// method for refreshing list after text input

		jlstStudentNamesList.setVisible(false);
		jlstStudentNamesList.removeAll();
		DefaultListModel<Student> listModel = new DefaultListModel<Student>();

		for (int i = 0; i < newList.size(); i++) {

			listModel.addElement(newList.get(i));
			jlstStudentNamesList.setModel(listModel);
			jlstStudentNamesList.setVisible(true);
		}

	}


	private void createMenu(final JTextField field, final JList<Student> list, JFrame frame,  SearchLogicHandler search ) {

		mainBar = new JMenuBar();
		fileMenu = new JMenu("File");
		editMenu = new JMenu("Edit");
		dataMenu = new JMenu("Data");

		mainBar.add(fileMenu);
		mainBar.add(editMenu);
		mainBar.add(dataMenu);
		quitItem = new JMenuItem("Quit Student Records");
		copyItem = new JMenuItem("Copy");
		pasteItem = new JMenuItem("Paste");
		deleteItem = new JMenuItem("Delete");
		loadExamResultsItem = new JMenuItem("Load Exam Results");
		loadAnonymousMarkingCodesItem = new JMenuItem("Load Anonymous Marking Codes");
		compareToAverageItem = new JMenuItem("Compare to Average");
		emailToStudentsItem = new JMenuItem("Email to Students");
		emailSettingsItem = new JMenuItem("Email Settings");
		scrapeKEATSitem=new JMenuItem("Fetch KEATS Participation");
		exportPDFitem=new JMenuItem("Export Student Data to PDF");
		editMenu.add(copyItem);
		editMenu.add(pasteItem);
		editMenu.add(deleteItem);
		fileMenu.add(loadAnonymousMarkingCodesItem);
		fileMenu.add(loadExamResultsItem);
		fileMenu.addSeparator();
		fileMenu.add(quitItem);
		dataMenu.add(emailToStudentsItem);
		dataMenu.add(emailSettingsItem);
		dataMenu.addSeparator();
		dataMenu.add(compareToAverageItem);
		dataMenu.add(scrapeKEATSitem);
		dataMenu.addSeparator();
		dataMenu.add(exportPDFitem);

		exportPDFitem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				PDFcheckWindow pdf=new PDFcheckWindow();
				pdf.setVisible(true);
			}
		});

		quitItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				System.exit(0);
			}

		});

		compareToAverageItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				try{
					int data = tabs.getSelectedIndex();
					new ScatterplotInterface(scatterplotData.get(data)).setVisible(true);
				}
				catch(NullPointerException npe){
					JOptionPane.showMessageDialog(null, "No Result tab selected.", "", JOptionPane.WARNING_MESSAGE);
				}

			}
		});

		emailToStudentsItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EmailInterface().setVisible(true);
			}
		});

		emailSettingsItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				new EmailSettingsDialogue().setVisible(true);
			}

		});

		loadAnonymousMarkingCodesItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MarkingCodeImporter csv = new MarkingCodeImporter();
				field.setText(null);
				JOptionPane.showMessageDialog(null, "Anonymous marking codes imported.\n\n"+csv.getSuccessfulImports()+"  codes were for known students;\n\n"+csv.getFailedImports()+" codes were for unknown students",
						"Import Summary", JOptionPane.INFORMATION_MESSAGE);
			}

		});
		loadExamResultsItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {

				try{
					ExamResultImport results=new ExamResultImport();

						addTab();
					if(tabs.isVisible() == false){
						makeTabsVisible();
					}
					if(hasRun == false){
						addTabClose();
					}
				}
				catch(Exception eiei){
					JOptionPane.showMessageDialog(null, "No file was chosen", "File Picker", JOptionPane.ERROR_MESSAGE);
					eiei.printStackTrace();
				}

			}

		});

		//Copies the currently selected Student's name, ID, email and tutor's email to clipboard.
		copyItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e){
				Clipboard systemClipboard = Toolkit.getDefaultToolkit ().getSystemClipboard ();
				String clipboardContents = list.getSelectedValue().getName() + " ID# " + 
						list.getSelectedValue().getID() + " Email: " +
						list.getSelectedValue().getEmail()+ " Tutor email: "+
						list.getSelectedValue().getTutorEmail();
				StringSelection stringToAdd = new StringSelection (clipboardContents);
				systemClipboard.setContents (stringToAdd, null);
			}
		});

		//Pastes whatever is in System clipboard directly into search textfield. This can be altered for something more useful (potentially).
		pasteItem.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				Transferable t = systemClipboard.getContents(this);
				if (t == null)
					return;
				try {
					field.setText((String) t.getTransferData(DataFlavor.stringFlavor));
				} catch (Exception ex){
					ex.printStackTrace();
				}//try
			}
		});

		scrapeKEATSitem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				fetchItem.makeWindow();
			}

		});
		frame.setJMenuBar(mainBar);

	}
}
