/**
 * Selected Student Interface Class
 */
package view.student;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import view.main.MainInterface;
import model.result.Result;
import model.student.Student;
import model.student.keatsEntry;

/**
 * 
 * @author Kyle Hodgetts
 * @author Alex Gubbay
 * @author Peter Barta
 * @version 1.1
 * 
 * <p>Displays a window with relevant information for the selected student</p>
 * @see MainInterface
 *
 */
@SuppressWarnings("serial")
public class SelectedStudent extends JFrame {

	private Student student;
	private JPanel upperPanel;
	private JPanel lowerPanel;
	private JTextPane txtStudentName;
	private JTextPane txtStudentEmail;
	private JTextPane txtStudentNo;
	private JTextPane txtTutorEmail;
	private Border border;
	private ArrayList<Result> resultsList;
	private DefaultTableModel resultModel;
	private JTable resultsTable;
	private JPanel lowerContainer;
	private ArrayList<keatsEntry> participationList;
	private JPanel tutorStudentPanel;
	private DefaultTableModel participationModel;
	private JTable participationTable;
	private JPanel participationPanel;

	/**
	 * Constructs the window with containing
	 * <ul>
	 * 		<li>The Student's Name</li>
	 * 		<li>The Student's Email Address</li>
	 * 		<li>The Student's ID Number</li>
	 * 		<li>The Student's Personal Tutor's Email Address</li>
	 * </ul>
	 * @param student 	Name and student number selected from list
	 */
	public SelectedStudent(Student student) {
		this.student = student;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setLayout(new BorderLayout());
		this.setResizable(false);
		border = new EmptyBorder(10,10,10,10);
		upperPanel = new JPanel(new BorderLayout());
		lowerPanel = new JPanel();
		SimpleAttributeSet centerAlign = new SimpleAttributeSet();
		StyleConstants.setAlignment(centerAlign, StyleConstants.ALIGN_CENTER);

		upperPanel.setForeground(Color.WHITE);
		upperPanel.setBackground(Color.WHITE);
		lowerPanel.setForeground(Color.WHITE);
		lowerPanel.setBackground(Color.WHITE);

		txtStudentName = new JTextPane();
		txtStudentName.setBorder(new CompoundBorder(txtStudentName.getBorder(), border));
		txtStudentName.setEditable(false);
		txtStudentName.setParagraphAttributes(centerAlign, false);
		txtStudentName.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		txtStudentName.setText("Name: "+student.getName());
		upperPanel.add(txtStudentName, BorderLayout.NORTH);

		txtStudentEmail = new JTextPane();
		txtStudentEmail.setBorder(new CompoundBorder(txtStudentEmail.getBorder(), border));
		txtStudentEmail.setEditable(false);
		txtStudentEmail.setParagraphAttributes(centerAlign, false);
		txtStudentEmail.setFont(new Font("Lucida Grande", Font.ITALIC, 18));
		txtStudentEmail.setText("Email: "+student.getEmail());
		upperPanel.add(txtStudentEmail, BorderLayout.SOUTH);

		txtStudentNo = new JTextPane();
		txtStudentNo.setBorder(new CompoundBorder(txtStudentNo.getBorder(), border));
		txtStudentNo.setParagraphAttributes(centerAlign, false);
		txtStudentNo.setFont(new Font("Lucida Grande", Font.PLAIN, 12));
		txtStudentNo.setEditable(false);
		txtStudentNo.setText("Student #: "+Integer.toString(student.getID()));
		//lowerPanel.add(txtStudentNo, BorderLayout.NORTH);

		txtTutorEmail = new JTextPane();
		txtTutorEmail.setBorder(new CompoundBorder(txtTutorEmail.getBorder(), border));
		txtTutorEmail.setParagraphAttributes(centerAlign, false);
		txtTutorEmail.setEditable(false);
		txtTutorEmail.setFont(new Font("Lucida Grande", Font.PLAIN, 12));
		txtTutorEmail.setText("Tutor: "+student.getTutorEmail());
		//lowerPanel.add(txtTutorEmail, BorderLayout.SOUTH);

		tutorStudentPanel=new JPanel();
		tutorStudentPanel.setLayout(new FlowLayout());
		tutorStudentPanel.add(txtStudentNo);
		tutorStudentPanel.add(txtTutorEmail);

		participationModel =new  DefaultTableModel(){
			public boolean isCellEditable(int row, int column)
			{
				return false;
			}
		};;
		participationTable =new JTable(participationModel);

		participationModel.addColumn("Module");
		participationModel.addColumn("Last Access");

		participationList=student.getParticipationData();
		for (int i = 0; i < participationList.size(); i++) {
			String[] participationRowData = {participationList.get(i).getModule(), participationList.get(i).getLastLogin()};
			participationModel.addRow(participationRowData);
			;}


		resultsList=student.getResults();
		resultModel = new DefaultTableModel(){
			public boolean isCellEditable(int row, int column)
			{
				return false;
			}
		};

		resultsTable=new JTable(resultModel);

		resultModel.addColumn("Module");
		resultModel.addColumn("Assessment");
		resultModel.addColumn("Mark");
		resultModel.addColumn("Grade");
		for (int i = 0; i < resultsList.size(); i++) {
			String[] resultsRowData={resultsList.get(i).getModuleCode(), resultsList.get(i).getAssessment(), 
					String.valueOf(resultsList.get(i).getMark()), resultsList.get(i).getGrade()};
			resultModel.addRow(resultsRowData);
		}

		participationPanel =new JPanel();
		participationPanel.add(participationTable.getTableHeader());
		participationPanel.add(new JScrollPane(participationTable));

		participationTable.setPreferredScrollableViewportSize(participationTable.getPreferredSize());
		participationTable.setFillsViewportHeight(true);
		participationPanel.setMinimumSize(new Dimension(120,120));
		participationPanel.setMaximumSize(new Dimension(120,120));
		participationTable.setMaximumSize(new Dimension(120, 120));




		lowerPanel.setLayout(new BoxLayout(lowerPanel, BoxLayout.X_AXIS));
		lowerPanel.add(tutorStudentPanel);
		lowerPanel.add(participationPanel);

		lowerContainer=new JPanel();
		lowerContainer.setLayout(new BoxLayout(lowerContainer, BoxLayout.Y_AXIS));
		lowerContainer.add(lowerPanel);
		lowerContainer.add(new JScrollPane(resultsTable));
		lowerContainer.add(resultsTable.getTableHeader());
		lowerContainer.add(resultsTable);
		this.add(upperPanel, BorderLayout.NORTH);
		this.add(lowerContainer, BorderLayout.SOUTH);
		this.setResizable(false);
		this.pack();

	}

}
