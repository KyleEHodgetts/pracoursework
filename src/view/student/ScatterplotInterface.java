package view.student;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JFrame;

import model.student.Student;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


/**
 * Displays scatter plot graph where the X and Y coordinates are determined
 * by the current module mark and the average of a Student's combined mark, respectively.
 * 
 * @author Kyle Hodgetts
 * @version 1.0
 * @see {@link http://www.jfree.org/jfreechart/}
 *
 */
public class ScatterplotInterface extends JFrame {

	private static final String title = "Compare to Average Assessment Mark";
	private static final String xLabel = "Average of Other Assessments"; 
	private static final String yLabel = "Currently Selected Assigment";

	private HashMap<Student, Double>data;
	private Set<Student> dataStudents;

	/**
	 * Constructs the scatter plot with the <code>data</code> passed to it.
	 * @param data	A collection of students with the current module mark.
	 */
	public ScatterplotInterface(HashMap<Student, Double> data) {
		super("Averages");
		this.data = data;
		this.dataStudents = data.keySet();
		final ChartPanel chartPanel = createScatterPanel();
		this.add(chartPanel, BorderLayout.CENTER);
		this.setLocationRelativeTo(null);
		this.setSize(700, 700);
	}

	private ChartPanel createScatterPanel() {
		JFreeChart jfreechart = ChartFactory.createScatterPlot(
				//Chart title
				title,
				//x-axis String label
				xLabel,
				//y-axis String label
				yLabel,
				//data to be added
				addSelectedData(),
				PlotOrientation.VERTICAL,
				//Include legend?
				true,
				//Include tooltips?
				true,
				//Include urls?
				false);

		XYPlot xyPlot = (XYPlot) jfreechart.getPlot();

		//Visible gridlines
		xyPlot.setDomainCrosshairVisible(true);
		xyPlot.setRangeCrosshairVisible(true);

		//Customization for graph axes 
		XYItemRenderer renderer = xyPlot.getRenderer();
		renderer.setSeriesPaint(0, Color.black);

		//Setting x-axis domain (0-100 for marks)
		NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
		domain.setRange(0, 100);
		domain.setTickUnit(new NumberTickUnit(5));
		domain.setVerticalTickLabels(true);

		//Setting y-axis domain (0-100 for marks)
		NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
		range.setRange(0, 100);
		range.setTickUnit(new NumberTickUnit(5));

		return new ChartPanel(jfreechart);
	}

	//Method to add the currently selected students' data to the graph
	private XYDataset addSelectedData() {
		XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
		XYSeries series = new XYSeries("Student");

		/*
		 * For each student in the collection, create a scatter plot entry
		 * for them which marks their mark average and the mark for the current module
		 * being displayed.
		 */
		for(Student s : dataStudents){
			series.add(new XYDataItem((double)data.get(s), s.getAverage()));
		}       
		xySeriesCollection.addSeries(series);

		return xySeriesCollection;
	}


}
