package view.student;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.student.KeatsScraper;


/**
 * GUI components for taking information from user to scrape HTML data from keats with.
 * Also contains erro catching and displaying blocks for common errors that may arrise
 * in the scrape process.
 * 
 * @author Alex Gubbay
 *
 */
@SuppressWarnings("serial")
public class fetchParticipationWindow extends JFrame {

	private JFrame parentFame;
	private String strUrlInput;
	private String strModuleCode;
	private String strUsername;
	private String strPassword;
	private JTextField jtfUrlEntry;
	private JTextField jtfModuleEntry;
	private JPanel jpUrlPanel; 
	private JPanel jpModulePanel;
	private JLabel jlUrlMessage;
	private JLabel jlModuleMessage;
	
	private JTextField jtfUsernameEntry;
	private JPasswordField jpfPasswordEntry;
	private JLabel jlUsernameMessage;
	private JLabel jlPasswordMessage;
	private JPanel jpMainPanel;
	private JCheckBox jcbCredentialsRequired;
	private JLabel jlCredentialsCheck;
	private JButton jbFinish;
	private KeatsScraper ksScraperLogic;
	private JFrame jfNextwindow;
	private JButton jbFetch;
	private JPanel jpNextMainPanel;
	private JPanel jpUsernameBox;
	private JPanel jpPasswordBox;
	private JPanel jpButtonPanel;
	
	public fetchParticipationWindow(JFrame parent) {
		this.parentFame = parent;
	}
/**
 * Creates the window that collects user input for the keats scrape and passes it to KeatsScraper.
 * @see KeatsScraper
 */
	public void makeWindow() {
		setTitle("Fetch Participation Data");
		setLocationRelativeTo(parentFame);
		jpMainPanel = new JPanel();
		jpMainPanel.setLayout(new BoxLayout(jpMainPanel, BoxLayout.Y_AXIS));

		jpUrlPanel = new JPanel();
		jpModulePanel = new JPanel();
		jbFetch = new JButton("Next");

		jpUrlPanel.setLayout(new BoxLayout(jpUrlPanel, BoxLayout.X_AXIS));
		jpModulePanel.setLayout(new BoxLayout(jpModulePanel, BoxLayout.X_AXIS));

		jtfUrlEntry = new JTextField();
		jtfModuleEntry = new JTextField();

		jtfUrlEntry.setColumns(20);
		jtfModuleEntry.setColumns(10);
		jtfUrlEntry.setMaximumSize(jtfUrlEntry.getPreferredSize());
		jtfModuleEntry.setMaximumSize(jtfModuleEntry.getPreferredSize());

		jlUrlMessage = new JLabel("Participation page URL");
		jlModuleMessage = new JLabel("Module Code");

		jpUrlPanel.add(jlUrlMessage);
		jpUrlPanel.add(jtfUrlEntry);
		jpUrlPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		jpModulePanel.add(jlModuleMessage);
		jpModulePanel.add(jtfModuleEntry);
		jpModulePanel.add(jbFetch);
		jpModulePanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		jbFetch.addActionListener(new ActionListener() {
			
			@Override
			
			/**
			 * Performs URL Input Validation and creates the next window which asks for login details:
			 * Creates URL from string input. Will throw a MalformedURLException if the URL is not valid,
			 * but does not catch spaces in the URL which is a problem because OSX has a habit of adding
			 * spaces before copied URLs. I have added my own check for this.
			 * 
			 *  @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 *  @see java.java.net.URL;
			 */
			
			public void actionPerformed(ActionEvent e) {
				
				strUrlInput=jtfUrlEntry.getText();
				
				try {
					/*just being used to check the URL is valid, not needed elsewhere
					 * as the HTML scraper takes a string.
					 */
					@SuppressWarnings("unused")
					URL checkValid=new URL(strUrlInput);
					if(strUrlInput.contains(" ")){
						throw new MalformedURLException();
					}
					
					strModuleCode=jtfModuleEntry.getText();
					if(strModuleCode.isEmpty()){
						jpModulePanel.setBackground(Color.decode("#FF8080"));
						return;
					}jpModulePanel.setBackground(null);
					loginDetailsWindow();
					checkValid=null;
					
				} catch (MalformedURLException e1) {

					jpUrlPanel.setBackground(Color.decode("#FF8080"));
					jlUrlMessage.setText("Invalid URL: ");
				}
				
				}
	});
	
	
		
		jpMainPanel.add(jpUrlPanel);
		jpMainPanel.add(jpModulePanel);
	
		this.add(jpMainPanel);
		pack();
		setVisible(true);
	}
	
	/**
	 * Creates the window and items which prompts user for KEATS login details. Closes old window and then calls calls the {@link KeatsScraper}
	 */
private	void loginDetailsWindow(){
	
	this.dispose();
	ksScraperLogic=new KeatsScraper();
	jfNextwindow = new JFrame();
	jpNextMainPanel =new JPanel();
	jpNextMainPanel.setLayout(new BoxLayout(jpNextMainPanel, BoxLayout.Y_AXIS));
	
	
	jpUsernameBox=new JPanel();
	jpUsernameBox.setLayout(new BoxLayout(jpUsernameBox, BoxLayout.X_AXIS));
	jpUsernameBox.setBorder(new EmptyBorder(10, 10, 10, 10));
	jlUsernameMessage= new JLabel("Username");
	jtfUsernameEntry= new JTextField(12);
	jpUsernameBox.add(jlUsernameMessage);
	jpUsernameBox.add(jtfUsernameEntry);
	
	
	jpPasswordBox=new JPanel();
	jpPasswordBox.setLayout(new BoxLayout(jpPasswordBox, BoxLayout.X_AXIS));
	jpPasswordBox.setBorder(new EmptyBorder(10, 10, 10, 10));
	jpfPasswordEntry = new JPasswordField(12);
	jlPasswordMessage= new JLabel("password");
	jpPasswordBox.add(jlPasswordMessage);
	jpPasswordBox.add(jpfPasswordEntry);
	
	
	
	jpButtonPanel =new JPanel();
	jpButtonPanel.setLayout(new BoxLayout(jpButtonPanel, BoxLayout.X_AXIS));
	jpButtonPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
	jcbCredentialsRequired=new JCheckBox();
	jlCredentialsCheck=new JLabel("Credentials are required to access this data");
	jbFinish=new JButton("Finish");
	jpButtonPanel.add(jlCredentialsCheck);
	jpButtonPanel.add(jcbCredentialsRequired);
	jpButtonPanel.add(jbFinish);
	
	jcbCredentialsRequired.setSelected(true);
	jcbCredentialsRequired.addActionListener(new ActionListener() {
		
		
		/**
		 * disables the user credentials if the "Credentials Required box is unchecked" and likewise,
		 * re-enables it if it is checked again.
		 *  @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(jcbCredentialsRequired.isSelected()){
				
				jpfPasswordEntry.setEnabled(true);
				jtfUsernameEntry.setEnabled(true);
				jlUsernameMessage.setEnabled(true);
				jlPasswordMessage.setEnabled(true);
			}
			if(!jcbCredentialsRequired.isSelected()){
				
				jpfPasswordEntry.setEnabled(false);
				jtfUsernameEntry.setEnabled(false);
				jlUsernameMessage.setEnabled(false);
				jlPasswordMessage.setEnabled(false);
			}
		}
	});
	
	jbFinish.addActionListener(new ActionListener() {
		
		/**
		 * Starts the dataScrape process with the data provided.
		 * 
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				if(jcbCredentialsRequired.isSelected()){
					
								if(jtfUsernameEntry.getText().isEmpty()){
									//checks if user has entered a username.
									jpUsernameBox.setBackground(Color.decode("#FF8080"));
									return;
								}
								jpUsernameBox.setBackground(null);
							strUsername=jtfUsernameEntry.getText();
							strPassword=String.copyValueOf(jpfPasswordEntry.getPassword());
							if(strPassword.equals("")){
								//checks if user has entered a password.
								jpPasswordBox.setBackground(Color.decode("#FF8080"));
								return;
							}
							ksScraperLogic.scrapeWithCredentials(strUrlInput, strUsername, strPassword, strModuleCode);
							jfNextwindow.dispose();
							//clears credentials for security.
							strPassword=" ";
							strUsername=" ";
								}
						
			
			
				//scrape without credentials
			if(!(jcbCredentialsRequired.isSelected())){
				
				ksScraperLogic.scrapeWithoutCredentials(strUrlInput, strModuleCode);
				jfNextwindow.dispose();
			}
			}
			catch (NullPointerException eNullPointer) {
				JOptionPane
						.showMessageDialog(
								null,
								"Error: Unable to collect data from page provided .\n Check URL and try again",
								"Connection Error", JOptionPane.ERROR_MESSAGE);
			}
			catch (UnknownHostException eUnknownHost) {
				JOptionPane
						.showMessageDialog(
								null,
								"Error: Unkown host. Double check address and internet connection",
								"Connection Error", JOptionPane.ERROR_MESSAGE);
			}
			catch (SocketTimeoutException eTimeout) {
				JOptionPane
						.showMessageDialog(
								null,
								"Error: Connection timeout.\n The server may be busy. Check internet connection and try again",
								"Connection Error", JOptionPane.ERROR_MESSAGE);
			} 
			catch (Exception eUnkownError) {
				JOptionPane
				.showMessageDialog(
						null,
						"Fatal Error.\n Check credentials and URL and try again",
						"Connection Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	});
	
	jpNextMainPanel.add(jpUsernameBox);
	jpNextMainPanel.add(jpPasswordBox);
	jpNextMainPanel.add(jpButtonPanel);
	jpUsernameBox.setMaximumSize(jpUsernameBox.getPreferredSize());
	jpPasswordBox.setMaximumSize(jpPasswordBox.getPreferredSize());
	
	jfNextwindow.setTitle("Fetch Participation Data");
	jfNextwindow.setLocationRelativeTo(parentFame);
	
	jfNextwindow.add(jpNextMainPanel);
	jfNextwindow.pack();
	jfNextwindow.setVisible(true);

}	

}

