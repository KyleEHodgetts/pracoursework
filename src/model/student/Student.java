package model.student;

import java.util.ArrayList;
import java.util.InputMismatchException;

import model.result.Result;

/**
 * @author 	Kyle Hodgetts
 * @author  Alexander Gubbay
 * @author 	Peter Barta
 * @version 1.0
 * 
 * <p>Represents a Student enrolled within the University</p>
 * <p>Each student has a: 
 * 		<ul>
 * 			<li>Name</li>
 * 			<li>Student Email Address</li>
 *  		<li>Anonymous Marking Code</li>
 * 			<li>Personal Tutor Email address associated with them</li>
 * 		</ul>
 * </p>
 */
public class Student 
{
	private String name;
	private int studentNumber;
	private String emailAddress;
	private String tutorEmailAddress;
	private String annonMarkingCode;
	private ArrayList<Result> results;
	private ArrayList<keatsEntry> participationRecords;
	private  Result blankResult;
	private keatsEntry blankKeats;

	/**
	 * <p>Student Class constructor</p>
	 * @param name 			The name which will refer to the student
	 * @param studentNumber A unique student number attached to the Student
	 * @param emailAddress 	A unique email address used to contact the Student.
	 * @throws InputMismatchException	When data of wrong type is passed into constructor.
	 */
	public Student(String name, int studentNumber, String emailAddress, String tutorEmailAddress) throws InputMismatchException
	{
		this.participationRecords=new ArrayList<keatsEntry>();
		this.blankKeats=new keatsEntry("n/a", "n/a", "n/a", "n/a");
		participationRecords.add(blankKeats);
		this.name = name;
		this.studentNumber = studentNumber;
		this.emailAddress = emailAddress;
		this.tutorEmailAddress = tutorEmailAddress;
		this.annonMarkingCode="";
		this.results=new ArrayList<Result>();
		this.blankResult=new Result("n/a","n/a","n/a",0,"n/a");
		results.add(blankResult);

	}

	/**
	 * Gets all student information and formats it into one string to be returned
	 * @return The Student name followed by the Student's email address and Student number
	 */
	public String toString()
	{
		return String.format("%s (%d)", this.name, this.studentNumber); 
	}

	/**
	 * 
	 * @return The name of the student
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * Returns the Student ID
	 * @return  Student ID Number
	 */
	public int getID()
	{
		return this.studentNumber;
	}

	/**
	 * Returns the Student's Email
	 * @return Student Email Address
	 */
	public String getEmail()
	{
		return this.emailAddress;
	}

	/**
	 * Returns the Student's Tutor's Email
	 * @return Student's Tutor's Email Address
	 */
	public String getTutorEmail()
	{
		return this.tutorEmailAddress;
	}
	/**
	 * Adds student anonymous marking code to student object.
	 * @param code anonymous marking code to be added to the student record.
	 */
	public void addAnnonMarkingCodeStudent(String code){
		this.annonMarkingCode = code;
	}

	/**
	 * Returns student anonymous code if present.
	 * @return student anonymous marking code.
	 */
	public String getAnnonMarkingCode(){
		return this.annonMarkingCode;
	}

	/**
	 * <p>Adds a new result to the student's list of results.</p>
	 * @param r	A Result
	 * @see Result
	 */
	public void addResult(Result r){
		if(results.get(0).equals(blankResult)){
			results.remove(0);
		}

		results.add(r);
	}


	/**
	 * @return Returns a collection of all the results this student has
	 * @see Result
	 */
	public ArrayList<Result> getResults(){

		return results;
	}

	/**
	 * 
	 * @param module Module code of participation data in string format.
	 * @param lastLogin time and date of last log in string format.
	 */
	public void addParticipationData(keatsEntry data){
		if(participationRecords.get(0).equals(blankKeats)){
			participationRecords.remove(0);
		}
		participationRecords.add(data);
	}

	/**
	 * @return the participation
	 */
	public ArrayList<keatsEntry> getParticipationData(){
		return participationRecords;

	}

	/**
	 * 
	 * @return <code>Student</code>'s average mark for all results submitted
	 */
	public double getAverage(){
		double markSum = 0;
		double resultAmount = results.size();

		for(Result r : results){
			markSum+= r.getMark();
		}
		return markSum/resultAmount;
	}
	/**
	 * Resets the <code> Student </code> to null, to stop multiple marking code sets
	 * running concurrently.
	 */
	public void removeMarkingCode(){
		this.annonMarkingCode=null;
	}






}

