package model.student;

/**
 * Object for each row of data from the HTML scrape from keats that contains paritipation data.
 * 
 * @author Alex Gubbay
 * @version 1.0
 *
 */
public class keatsEntry {

	private String name;
	private String lastLogin;
	private String email;
	private String module;

	/**
	 * 
	 * @param name			Name of the Student
	 * @param email			Student's Email.
	 * @param lastLogin		The last login the student made
	 * @param module		The module.
	 */
	public keatsEntry(String name, String email, String lastLogin, String module){
		this.name=name;
		this.email=email;
		this.lastLogin=lastLogin;
		this.module=module;
	}

	/**
	 * @return the module
	 */
	public String getModule() {
		return module;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the lastLogin
	 */
	public String getLastLogin() {
		return lastLogin;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}




}
