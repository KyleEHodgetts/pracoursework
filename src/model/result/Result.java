package model.result;

import java.util.InputMismatchException;

/**
 * 
 * @author Kyle Hodgetts
 * @author Alex Gubbay
 * @author Peter Barta
 * @version 1.0
 * <p>Class for storing a students result as taken from a particular row of a text file</p>
 */
public class Result {
	private String moduleCode;
	private String assessment;
	private String candKey;
	private int mark;
	private String grade;
	private String name;

	/**
	 * <p>Class Constructor</p>
	 * @param moduleCode				Subject's Module Code
	 * @param assessment				Assessment number for module. A module may have more than one assessment method, such as exam and course work
	 * @param candKey					Candidate key. This could either be an anonymous marking code(Exam) or the students assigned student number (Course work)
	 * @param mark						The mark the student has gained
	 * @param grade						AB, if absent. D, if deferred. W, if student has withdrawn. F, if present and mark is 0. Blank otherwise 
	 * @throws InputMismatchException	When data type doesn't match the data types defined in the constructor
	 */
	public Result(String moduleCode, String assessment, String candKey, int mark, String grade) throws InputMismatchException{
		this.moduleCode = moduleCode;
		this.assessment = assessment;
		this.candKey = candKey;
		this.mark = mark;
		this.grade = grade;
		this.name=null;
	}

	/**
	 * @return Module code for this result
	 */
	public String getModuleCode(){
		return this.moduleCode;
	}

	/**
	 * @return Assessment number for this result
	 */
	public String getAssessment(){
		return this.assessment;
	}

	/**
	 * @return Candidate key for the student for whom this result belongs to
	 */
	public String getCandidateKey(){
		return this.candKey;
	}

	/**
	 * 
	 * @return Return name of student if known, else empty string.
	 */
	public String getName(){
		if(this.name!=null){
			return this.name;
		}else return "";
	}
	/**
	 * 
	 * @param studentNumber student number to replace candidate key with
	 */
	public void setDeannonymisedKey(int studentNumber){
		this.candKey=String.valueOf(studentNumber);
	}
	/**
	 * 
	 * @param studentName name of student to be added to result.
	 */
	public void setName(String studentName){
		this.name=studentName;
	}

	/**
	 * @return mark for this result
	 */
	public int getMark(){
		return this.mark;
	}

	/**
	 * @return grade for this result. Commonly blank
	 */
	public String getGrade(){
		return this.grade;
	}

	/**
	 * @return the result row as taken from the .csv file
	 */
	@Override
	public String toString(){
		return String.format("%s %s %s %d %s", this.moduleCode, this.getAssessment(), this.candKey, this.mark, this.getGrade());
	}

	/**
	 * 
	 * @return <code>Result</code> in a natural readable format.
	 */
	public String toFormattedString(){
		String toReturn = ("Module Code: "+ this.moduleCode+
				" Assessment Number "+ this.getAssessment()+ " with mark "+ this.mark);
		if(this.mark == 0){
			toReturn += ". Grade Recieved: " + this.getGrade();
		}

		return toReturn;
	}

}
