package model.result;

import java.util.ArrayList;

import model.student.Student;
import controller.student.StudentDataManager;

/**
 * 
 * @author Kyle Hodgetts
 * @author Alex Gubbay
 * @author Peter Barta
 * @version 1.0
 * 
 * Each assessment is a collection of results
 * @see Result
 */
public class Assessment {
	private ArrayList<Result> results;
	private String AssesmentCode;

	/**
	 * Assessment constructor
	 */
	public Assessment(){
		results = new ArrayList<Result>();
	}

	/**
	 * 
	 * @param r	A result to be added to the assessment
	 * 
	 */
	public void addResult(Result r){
		Student tempStudent=null;
		//if its coursework
		if(!(r.getName().equals(""))){
			tempStudent=StudentDataManager.getStudent(r.getName());
		}
		//if its not coursework
		else{
			tempStudent=StudentDataManager.getStudentByannonCode(r.getCandidateKey());
		}
		//matching student found
		if(!(tempStudent==null)){
			r.setDeannonymisedKey(tempStudent.getID());
			tempStudent.addResult(r);
			r.setName(tempStudent.getName());
			AssesmentCode=r.getAssessment();
		}
		else{
		}
		results.add(r);
	}

	/**
	 * @return the Code of the assessment.
	 */
	public String getAssessmentCode(){
		return this.AssesmentCode;
	}
	/**
	 * Gets number of results in the assessment
	 * @return int number of results
	 */
	public int getNumberOfResults(){
		return	results.size();
	}
	/**
	 * Gets the result at the location i
	 * @param i the loction from which to get the result
	 * @return the result found at the location
	 */
	public Result get(int i){
		return results.get(i);
	}
	/**
	 * Gets the module code of the assessment.
	 * @return The module code.
	 */
	public String getModuleCode(){
		return results.get(0).getModuleCode();
	}


}
