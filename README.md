# README #

For the attention of the marker:

Here is the list of the external libraries we are using, should you wish to run the program from the command lines.

- ini4j-0.5.4-jdk14.jar
- ini4j-0.5.4.jar
- javax.mail.jar
- jcommon-1.0.23.jar
- jfreechart-1.0.19.jar
- jsoup-1.8.1.jar
- pdfbox-app-1.8.8.jar
- StudentData.jar

The main method can be found in src/main/MainApp.java

	On Launch:
	The left table will be populated with students, double clicking will open a window containing information containing just that 
	student.
	
	Uploading marking codes and results:
	You can only upload one set of marking codes per student, uploading more than one will cause conflicts.
	Each time results are successfully uploaded, a tab will open containing the students who were down to the take the exam
	and the mark they received.
	
	Viewing average marks:
	Depending on the tab selected, when you select Data -> Compare to average, a new scatterplot chart will be generated that
	will show the students mark for the current selected tab (x-axis) with the average for all known module results (y-axis).
	
	Email to students: 
	You can email results to students through Data -> Email to Students.
	You can select the students you wish to send them to by selecting the checkbox next to the students name and it will add them
	to the mailing list. You can also write text to appear before and after the student mark, each email sent will be unique to 	the student.
	
	Pressing send will commence the email sending process, if the password and email settings are valid.
	
	Changing Email Settings:
	Under Data -> Email Settings you can edit the email configurations to your choosing.
	You will need to use a valid SMTP server such as 'smtp.gmail.com'
	You will need a valid port number such as '587' for TLS connections or '465' for SSL
	You will also need a valid user name which is the email address without the '@' and what follows.
	For example, the email 'mrsmith@gmail.com', the username will be 'mrsmith'
	The email domain will need to match the server you are using.
	
	Fetching KEATs participation:
	Data -> Fetch Participation
	
	You will need to enter a valid page URL that you want to fetch student participation from
	e.g. 'http://keats.kcl.ac.uk/mod/page/view.php?id=886138'
	and the module code. e.g. '4CCS1ELA' for Elementary Logic with Applications
	
	Then you will need to supply valid KEATs login credentials to proceed with the scraping.
	
	Exporting Student data to PDF:
	Data -> Export Student Data to PDF
	
	Select the students you wish to export data for.
	Then select the directory you wish to export to
	Finally, press Export.
	
